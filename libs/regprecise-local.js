const regulonDbBaseDir = './db/regulondb/';

const fs = require('fs');
const path = require('path');
const AdmZip = require('adm-zip');

const baseDir = './db/regprecise/';

const dbZip = new AdmZip(path.join(baseDir, 'regprecise.json.zip'));
dbZip.extractAllTo(baseDir);

const parse = (file) => JSON.parse(fs.readFileSync(path.join(baseDir, file)));

const regulondbParse = (file) => JSON.parse(fs.readFileSync(path.join(regulonDbBaseDir, file)));

const regulonDb = {
    regulonNetwork: regulondbParse('regulon-network.json')
};

const regpreciseDb = {
    genomes: parse('genomes.json'),
    genes: parse('genes.json'),
    regulons: parse('regulons.json'),
    regulogs: parse('regulogs.json'),
    regulators: parse('regulators.json'),
    sites: parse('sites.json')
};

const gotermZip = new AdmZip('./db/go/rp-gene-terms.json.zip');
gotermZip.extractAllTo('./db/go/');

const goTerms = JSON.parse(fs.readFileSync('./db/go/rp-gene-terms.json'));

const kmeans = require('node-kmeans');

module.exports.genomes = (filter, cb) => {
    cb(null, regpreciseDb.genomes.filter(filter));
};

module.exports.genes = (filter, cb) => {
    cb(null, regpreciseDb.genes.filter(filter));
};

module.exports.regulogs = (filter, cb) => {
    cb(null, regpreciseDb.regulogs.filter(filter));
};

module.exports.regulons = (filter, cb) => {
    cb(null, regpreciseDb.regulons.filter(filter));
};

module.exports.regulators = (filter, cb) => {
    cb(null, regpreciseDb.regulators.filter(filter));
};

module.exports.sites = (filter, cb) => {
    cb(null, regpreciseDb.sites.filter(filter));
};

module.exports.status = (cb) => cb(null, 200);

module.exports.getTFNetwork = (genomeId, cb) => {
    let goTermsSet = new Set();
    let regulons = regpreciseDb.regulons.filter(r => r.genomeId === genomeId);
    regulons.map(regulon => {
        let targets = regpreciseDb.genes.filter(g => g.name && g.vimssId && g.regulonId === regulon.regulonId);
        regulon.targetGenes = [];
        regulon.targetRegulators = [];
        targets.forEach(target => {
            if (regulons.find(r => r.regulatorName.toLowerCase() === target.name.toLowerCase())) {
                let go = goTerms.find(gt => gt.geneName && gt.term && gt.geneName.toLowerCase() === target.name.toLowerCase());
                const goTerm = go && go.term ? go.term : 'go term unknown';
                target.regulatorName = target.name;
                goTermsSet.add(goTerm);
                regulon.targetRegulators.push({
                    function: target.function,
                    locusTag: target.locusTag,
                    regulonId: target.regulonId,
                    vimssId: target.vimssId,
                    regulatorName: target.name,
                    goTerm: goTerm
                });
            } else {
                regulon.targetGenes.push(target);
            }
        });
        return regulon;
    });

    regulons = regulons.filter(r => r.regulatorName);
    const data = {
        network: regulons,
        goTerms: Array.from(goTermsSet)
    };
    cb(null, data);
};

module.exports.getPathway = (regulonIdString, cb) => {
    let regulonIds = regulonIdString.split(' ');
    let network = FindPathwayNetwork(regulonIds);
    cb(null, network);
};

module.exports.getPathways = (regulonIdStrings, cb) => {
    let networks = [];
    regulonIdStrings.forEach(regulonIds => {
        networks.push(FindPathwayNetwork(regulonIds));
    });
    cb(null, networks);
};

// Find the genomes that match the selected pathway
module.exports.getPathwayGenomes = (network, cb) => {
    let regulons = [];
    network['selected-regulons'].forEach(tf => {
        let regulog = regpreciseDb.regulogs.find(r => r.regulogId === tf.regulogId);
        let rs = regpreciseDb.regulons.filter(r => r.regulogId === regulog.regulogId && tf.genomeId !== r.genomeId);
        regulons.push(...rs);
    });

    let genomeIds = new Set();
    regulons.forEach(r => {
        genomeIds.add(r.genomeId);
    });
    let regpreciseGenomes = [];
    genomeIds.forEach(g => {
        if (regulons.filter(r => r.genomeId === g).length === network['selected-regulons'].length) {
            let genome = {
                ...regpreciseDb.genomes.find(genome => genome.genomeId === g),
                regulons: regulons.filter(r => r.genomeId === g).map(r => r.regulonId)
            };
            regpreciseGenomes.push(genome);
        }
    });
    network['regprecise-genomes'] = regpreciseGenomes;

    let regulonDBGenome = {
        genomeName: 'K12',
        genomeId: 'regulonDB',
        regulons: []
    };

    network['selected-regulons'].forEach(regulon => {
        let matchedRegulon = regulonDb.regulonNetwork.find(r => r.regulatorName.toLowerCase() === regulon.regulatorName.toLowerCase());
        if (matchedRegulon) {
            regulonDBGenome.regulons.push(matchedRegulon.regulatorName);
        }
    });

    if (regulonDBGenome.regulons.length === network['selected-regulons'].length) {
        network['regulondb-genome'] = regulonDBGenome;
    }
    cb(null, network);
};

module.exports.getRegulogNetwork = (regulonId, cb) => {
    let network = {};
    let regulon = regpreciseDb.regulons.find(r => r.regulonId === regulonId);
    network['selected-regulon'] = regulon.regulonId;
    network['regulons'] = regpreciseDb.regulons.filter(r => r.regulogId === regulon.regulogId);
    network['selected-genome'] = regpreciseDb.genomes.find(g => g.genomeId === regulon.genomeId);

    for (let regulon of network['regulons']) {
        let genes = regpreciseDb.genes.filter(g => g.regulonId === regulon.regulonId);
        genes = genes.filter(g => g.name !== undefined);

        regulon.regulator = genes.find(g => g.name && g.name.toLowerCase() === regulon.regulatorName.toLowerCase());
        regulon.targetGenes = genes.filter(g => g.name && g !== regulon.regulator);

        for (let gene of regulon.targetGenes) {
            gene.sites = regpreciseDb.sites.filter(s => s.geneVIMSSId === gene.vimssId);
            gene.term = goTerms.find(t => t.geneName === gene.name).term;
        }
    }

    // Calculate and add distances...

    let uniqueGeneNames = network['regulons'].map(r => r.targetGenes.map(tg => tg.name))
        .reduce((a, b) => a.concat(b), [])
        .filter((name, index, self) => self.indexOf(name) === index)
        .sort((a, b) => a.localeCompare(b));

    let binaryGeneMatrix = generateBinaryGeneMatrix(uniqueGeneNames, network['regulons']);
    let target = binaryGeneMatrix[regulonId];

    for (let key of Object.keys(binaryGeneMatrix)) {
        let regulon = network['regulons'].find(r => r.regulonId === key);
        regulon.hammingDist = hammingDist(target, binaryGeneMatrix[key]);
        regulon.levensteinDist = levensteinDist(target, binaryGeneMatrix[key]);
    }

    network.binaryGeneMatrix = binaryGeneMatrix;

    network.regulons = network.regulons.sort((a, b) => a.hammingDist - b.hammingDist);

    // Adding these for access purposes
    network['regulog'] = regpreciseDb.regulogs.find(r => r.regulogId === regulon.regulogId);
    network['regulators'] = [];
    network['genomes'] = [];
    for (let regulon of network['regulons']) {
        for (let regulator of regpreciseDb.regulators.filter(r => r.regulonId === regulon.regulonId)) {
            network.regulators.push(regulator);
        }
        for (let genome of regpreciseDb.genomes.filter(g => g.genomeId === regulon.genomeId)) {
            network.genomes.push(genome);
        }
    }

    cb(null, network);
};

module.exports.kMeansCluster = (binaryGeneMatrix, k, cb) => {
    let vectors = [];

    for (let key of Object.keys(binaryGeneMatrix)) {
        vectors.push(binaryGeneMatrix[key].split(''));
    }

    kmeans.clusterize(vectors, { k: k }, (err, res) => {
        cb(err, res ? res.map(c => c.clusterInd.map(i => Object.keys(binaryGeneMatrix)[i])) : null);
    });
};

function generateBinaryGeneMatrix(geneNames, regulons) {
    let matrix = {};

    for (let regulon of regulons) {
        let vector = '';
        for (let geneName of geneNames) {
            vector += regulon.targetGenes.find(g => g.name === geneName) ? '1' : '0';
        }
        matrix[regulon.regulonId] = vector;
    }

    return matrix;
}

function hammingDist(a, b) {
    let dist = 0;

    for (let i = 0; i < a.length; i++) {
        if (a[i] !== b[i]) dist++;
    }

    return dist;
}

function levensteinDist(a, b) {
    let m = []; let i; let j; let min = Math.min;

    if (!(a && b)) return (b || a).length;

    for (i = 0; i <= b.length; m[i] = [i++]);
    for (j = 0; j <= a.length; m[0][j] = j++);

    for (i = 1; i <= b.length; i++) {
        for (j = 1; j <= a.length; j++) {
            m[i][j] = b.charAt(i - 1) === a.charAt(j - 1)
                ? m[i - 1][j - 1]
                : m[i][j] = min(
                    m[i - 1][j - 1] + 1,
                    min(m[i][j - 1] + 1, m[i - 1][j]));
        }
    }

    return m[b.length][a.length];
}

function FindPathwayNetwork(regulonIds) {
    let network = {};
    let goTermsSet = new Set();
    network['selected-regulons'] = [];
    // Find the targets and regulators for each selected TF
    regulonIds.forEach(regulonId => {
        let selectedRegulon = regpreciseDb.regulons.find(r => r.regulonId === regulonId);

        /// find regulating tfs (including self regulation)
        let regulatingTFs = [];
        // find all regulating genes
        let regulators = regpreciseDb.genes.filter(g => g.name && g.name.toLowerCase() === selectedRegulon.regulatorName.toLowerCase());

        // Check if the TF is self regulated, if so add it self to the target TFs
        regulators.forEach(regulator => {
            let tf = regpreciseDb.regulons.find(r => regulator.regulonId === r.regulonId && r.genomeId === selectedRegulon.genomeId);
            if (tf) {
                let go = goTerms.find(gt => gt.geneName && gt.geneName.toLowerCase() === tf.regulatorName.toLowerCase());
                const goTerm = go && go.term ? go.term : 'go term unknown';
                goTerm && goTermsSet.add(goTerm);
                regulatingTFs.push({
                    ...tf,
                    goTerm: goTerm,
                    name: tf.regulatorName,
                    regulatorName: undefined
                });
            }
        });

        /// Find target TFs
        let targetTFs = [];
        // Find all genes that the TF regulates
        let genes = regpreciseDb.genes.filter(g => g.name && g.regulonId === selectedRegulon.regulonId && g.name !== selectedRegulon.regulatorName);
        // Find the genes that match a TF
        genes = genes.map(g => {
            let tf = regpreciseDb.regulons.find(r => r.regulatorName.toLowerCase() === g.name.toLowerCase() && r.genomeId === selectedRegulon.genomeId);
            const go = goTerms.find(t => t.geneName && t.geneName.toLowerCase() === g.name.toLowerCase());
            const goTerm = go && go.term ? go.term : 'go term unknown';
            g.goTerm = goTerm;
            goTermsSet.add(goTerm);

            if (tf) {
                targetTFs.push({
                    ...tf,
                    name: tf.regulatorName,
                    regulatorName: undefined,
                    goTerm: goTerm
                });
            }
            return g;
        });

        // Find target genes
        let targetGenes = genes.filter(g => g.name && !targetTFs.find(r => r.name.toLowerCase() === g.name.toLowerCase()));
        let selected = {
            ...selectedRegulon,
            targetGenes: targetGenes,
            targetTFs: targetTFs,
            regulatingTFs: regulatingTFs
        };
        network['selected-regulons'].push(selected);
        network['goTerms'] = Array.from(goTermsSet);
    });
    return network;
}
