// const request = require('request');
const regulonDbBaseDir = './db/regulondb/';
const regpreciseBaseDir = './db/regprecise/';

const AdmZip = require('adm-zip');
const fs = require('fs');
const path = require('path');
const regulondbParse = (file) => JSON.parse(fs.readFileSync(path.join(regulonDbBaseDir, file)));

const regulonDb = {
    regulonNetwork: regulondbParse('regulon-network.json')
};

const regpreciseParse = (file) => JSON.parse(fs.readFileSync(path.join(regpreciseBaseDir, file)));

const regpreciseDb = {
    regulons: regpreciseParse('regulons.json'),
    genomes: regpreciseParse('genomes.json')
};

const gotermZip = new AdmZip('./db/go/rp-gene-terms.json.zip');
gotermZip.extractAllTo('./db/go/');

const goTerms = JSON.parse(fs.readFileSync('./db/go/rp-gene-terms.json'));

module.exports.getRegulators = (cb) => {
    return cb(null, regulonDb.regulonNetwork);
    // Removed during dev - need to put back when in prod
    // request({
    //     method: 'GET',
    //     url: `http://regulondb.ccg.unam.mx/webresources/network/getTFGene`
    // }, (error, response, body) => {
    //     if (error || response.statusCode !== 200) return cb(new Error(error || response.statusMessage));
    //     body = JSON.parse(body);
    //     let regulators;
    //     if (body && body.netWorkList) {
    //         regulators = mapRegulators(body.netWorkList);
    //     }
    //     if (!regulators) return cb(null, null);
    //     return cb(null, regulators);
    // });
};

// Get the regulon network
module.exports.getRegulonNetwork = (regulators, cb) => {
    let network = [];
    let regulatorNames = regulators.map(regualtor => regualtor.regulatorName.toLowerCase());
    let effects = new Set();
    let evidenceCodes = new Set();
    let goTermsSet = new Set();
    // Remove unneaded target genes and remove flatten their evidence and effects
    network = regulators.map(regulator => {
        let targetGenes = [];
        let targetRegulators = [];
        regulator.targetGenes.forEach(gene => {
            if (regulatorNames.find(r => r === gene.name.toLowerCase())) {
                let goTerm = goTerms.find(gt => gt.geneName && gt.term && gt.geneName.toLowerCase() === gene.name.toLowerCase());
                targetRegulators.push({
                    ...gene,
                    goTerm: goTerm && goTerm.term ? goTerm.term : 'go term unknown'
                });
                goTermsSet.add(goTerm && goTerm.term ? goTerm.term : 'go term unknown');
            } else {
                targetGenes.push({ name: gene.name });
            }
        });
        let regulatorEffects = new Set();
        let regulatorEvidenceCodes = new Set();
        let regulatorStrengths = new Set();
        regulator.targetGenes.forEach(tg => {
            regulatorEffects.add(tg.effect);
            effects.add(tg.effect);
            tg.evidence.forEach(e => {
                regulatorEvidenceCodes.add(e);
                evidenceCodes.add(e);
            });
            regulatorStrengths.add(tg.strength);
        });

        let regulatorData = {
            regulonId: regulator.regulatorName.toLowerCase(),
            regulatorName: regulator.regulatorName,
            effects: Array.from(regulatorEffects),
            evidenceCodes: Array.from(regulatorEvidenceCodes),
            strengths: Array.from(regulatorStrengths),
            targetGenes: targetGenes,
            targetRegulators: targetRegulators.map(r => {
                return {
                    regulatorName: r.name,
                    effect: r.effect,
                    strength: r.strength,
                    evidence: r.evidence,
                    goTerm: r.goTerm || 'go term unknown'
                };
            })
        };
        return regulatorData;
    });
    const data = {
        network: network,
        effects: Array.from(effects),
        evidenceCodes: Array.from(evidenceCodes),
        goTerms: Array.from(goTermsSet)
    };
    return cb(null, data);
};

module.exports.getGenes = (cb) => {
    return cb(null, regulonDb.regulonNetwork);
};

// Get the network for the regulons parsed from the regulonIdString
module.exports.getPathway = (regulonIdString, cb) => {
    let regulonIds = regulonIdString.split(' ');
    let network = FindPathwayNetwork(regulonIds);
    cb(null, network);
};

module.exports.getPathways = (regulonIdStrings, cb) => {
    let networks = [];
    regulonIdStrings.forEach(regulonIds => {
        networks.push(FindPathwayNetwork(regulonIds));
    })
    cb(null, networks);
};

/**
 *
 * @param {The regulon id's that the network nodes will represent} regulonIds
 */
function FindPathwayNetwork(regulonIds) {
    let network = {};
    network['selected-regulons'] = [];
    let regulonIdsCleaned = regulonIds.map(r => r.toLowerCase());
    let regulons = regpreciseDb.regulons;
    let effects = new Set();
    let evidenceCodes = new Set();
    let goTermsSet = new Set();
    // Need to loop through and find all genomes that have all selected ids

    let regpreciseGenomes = [];
    regpreciseDb.genomes.forEach(genome => {
        let matched = true;
        let matchedRegulons = [];
        for (let id of regulonIdsCleaned) {
            let regulon = regulons.find(r => genome.genomeId === r.genomeId && r.regulatorName.toLowerCase() === id);
            if (regulon) {
                matchedRegulons.push(regulon.regulonId);
            } else {
                matched = false;
                break;
            }
        }

        if (matched) {
            regpreciseGenomes.push({
                genomeId: genome.genomeId,
                genomeName: genome.name,
                regulonIds: matchedRegulons
            });
        }
    });

    // Find the targets and regulators for each selected TF
    regulonIds.forEach(regulonId => {
        let selectedRegulon = regulonDb.regulonNetwork.find(r => r.regulatorName.toLowerCase() === regulonId.toLowerCase());
        // Find Target TFs
        let targetTFs = [];
        regulonDb.regulonNetwork.forEach(regulator => {
            let tg = selectedRegulon.targetGenes.find(g => regulator.regulatorName.toLowerCase() === g.name.toLowerCase());
            if (tg) {
                let go = goTerms.find(gt => gt.geneName && gt.geneName.toLowerCase() === tg.name.toLowerCase() && gt.term);
                tg = {
                    ...tg,
                    goTerm: go && go.term ? go.term : 'go term unknown'
                };
                goTermsSet.add(tg.goTerm);
                effects.add(tg.effect);
                targetTFs.push(tg);
                tg.evidence.forEach(e => {
                    evidenceCodes.add(e);
                });
            }
        });

        // Check if the TF is self regulated, if so add it self to the target TFs
        let regulatingTFs = [];
        selectedRegulon.targetGenes.forEach(gene => {
            if (selectedRegulon.regulatorName.toLowerCase() === gene.name.toLowerCase()) {
                let go = goTerms.find(gt => gt.geneName && gt.geneName.toLowerCase() === gene.name.toLowerCase() && gt.term);
                regulatingTFs.push({
                    ...gene,
                    goTerm: go && go.term ? go.term : 'go term unknown'
                });
                gene.goTerm && goTermsSet.add(gene.goTerm);
                effects.add(gene.effect);
                gene.goTerm && goTermsSet.add(gene.goTerm);

                gene.evidence.forEach(e => {
                    evidenceCodes.add(e);
                });
            }
        });

        // Find target genes
        let targetGenes = selectedRegulon.targetGenes.filter(g => g.name && !targetTFs.find(r => r.name.toLowerCase() === g.name.toLowerCase())).map(g => {
            const go = goTerms.find(gt => gt.geneName && gt.geneName.toLowerCase() === g.name.toLowerCase() && gt.term);
            const goTerm = go && go.term ? go.term : 'go term unknown';
            return {
                ...g,
                goTerm: goTerm
            };
        });
        targetGenes.forEach(gene => {
            gene.goTerm && goTermsSet.add(gene.goTerm);
            effects.add(gene.effect);
            gene.evidence.forEach(e => {
                evidenceCodes.add(e);
            });
        });

        let selected = {
            ...selectedRegulon,
            targetGenes: targetGenes,
            targetTFs: targetTFs,
            regulatingTFs: regulatingTFs
        };
        network['selected-regulons'].push(selected);
        network['repgrecise-genomes'] = regpreciseGenomes;
        network['effects'] = Array.from(effects);
        network['evidenceCodes'] = Array.from(evidenceCodes);
        network['goTerms'] = Array.from(goTermsSet);
    });
    return network;
}

function mapRegulators(network) {
    let regulators = [];
    network.forEach(regulator => {
        let regulatorName = regulator.regulatorName;
        let targetGene = regulator.regulatedName;
        let effect = regulator.function;
        let evidence = regulator.evidenceCode.replace('[', '').replace(']', '').split(', ');
        let strength = regulator.evidenceType;

        let index = regulators.findIndex(r => r.regulatorName === regulatorName);
        if (index === -1) {
            regulators.push({ regulatorName: regulatorName, targetGenes: [] });
            index = regulators.length - 1;
        }
        regulators[index].targetGenes.push({
            name: targetGene,
            effect: effect,
            strength: strength,
            evidence: evidence
        });
    });
    return regulators;
}
