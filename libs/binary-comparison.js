/**
 * Compares the pathways for regprecise and regulon db
 * @param {PathwayNetwork[]} networks Contains the regulondb and regprecise pahtways
 * @param {string} type The type of binary operation AND, OR, XOR
 */
module.exports.binaryComparison = function(networks, type, cb) {
    let binaryNetwork = {};
    if (!networks) cb(null, []);
    switch (type) {
    case 'OR':
        // Find the regprecise network
        if (networks.length > 0) {
            binaryNetwork['selected-regulons'] = networks[0]['selected-regulons'].map(selectedRegulon => {
                let matchedGenes = [];
                let matchedRegulatingTfs = [];
                let matchedTargetTfs = [];

                for (let network of networks) {
                    let orGenes = network['selected-regulons'].find(r => r.regulatorName === selectedRegulon.regulatorName).targetGenes.filter(g => !matchedGenes.map(mg => mg.name.toLowerCase()).includes(g.name.toLowerCase()));
                    matchedGenes.push(...orGenes);

                    let orTargetTFs = network['selected-regulons'].find(r => r.regulatorName.toLowerCase() === selectedRegulon.regulatorName.toLowerCase()).targetTFs.filter(tf => !matchedTargetTfs.map(mtf => mtf.name.toLowerCase()).includes(tf.name.toLowerCase()));
                    matchedTargetTfs.push(...orTargetTFs);

                    let orRegulatingTFs = network['selected-regulons'].find(r => r.regulatorName.toLowerCase() === selectedRegulon.regulatorName.toLowerCase()).regulatingTFs.filter(tf => !matchedRegulatingTfs.map(mtf => mtf.name.toLowerCase()).includes(tf.name.toLowerCase()));
                    matchedRegulatingTfs.push(...orRegulatingTFs);
                }

                return {
                    regulatorName: selectedRegulon.regulatorName,
                    targetGenes: matchedGenes,
                    regulatingTFs: matchedRegulatingTfs,
                    targetTFs: matchedTargetTfs
                };
            });
        }
        break;
    case 'AND':
        // need to loop through and only add the node if it exists in the other genomes' networks
        // Only need to compare the first array to the rest because if its not in the first it cant match the AND statement
        if (networks.length > 0) {
            binaryNetwork['selected-regulons'] = networks[0]['selected-regulons'].map(selectedRegulon => {
                let matchedGenes = [];
                let matchedRegulatingTfs = [];
                let matchedTargetTfs = [];

                // check for genes
                selectedRegulon.targetGenes.forEach(gene => {
                    let found = true;
                    for (let i = 1; i < networks.length; i++) {
                        found = !!networks[i]['selected-regulons'].find(r => r.regulatorName.toLowerCase() === selectedRegulon.regulatorName.toLowerCase()).targetGenes.find(g => gene.name.toLowerCase() === g.name.toLowerCase());
                        if (!found) break;
                    }
                    if (found) matchedGenes.push(gene);
                });

                // check for regulating regulons
                selectedRegulon.regulatingTFs.forEach(tf => {
                    let found = true;
                    for (let i = 1; i < networks.length; i++) {
                        found = !!networks[i]['selected-regulons'].find(r => r.regulatorName.toLowerCase() === selectedRegulon.regulatorName.toLowerCase()).regulatingTFs.find(r => tf.name.toLowerCase() === r.name.toLowerCase());
                        if (!found) break;
                    }
                    if (found) matchedRegulatingTfs.push(tf);
                });

                // check for target regulons
                selectedRegulon.targetTFs.forEach(tf => {
                    let found = true;
                    for (let i = 1; i < networks.length; i++) {
                        found = !!networks[i]['selected-regulons'].find(r => r.regulatorName.toLowerCase() === selectedRegulon.regulatorName.toLowerCase()).targetTFs.find(r => tf.name.toLowerCase() === r.name.toLowerCase());
                        if (!found) break;
                    }
                    if (found) matchedTargetTfs.push(tf);
                });
                return {
                    regulatorName: selectedRegulon.regulatorName,
                    targetGenes: matchedGenes,
                    regulatingTFs: matchedRegulatingTfs,
                    targetTFs: matchedTargetTfs
                };
            });
        }

        break;
    case 'XOR':
        if (networks.length > 0) {
            binaryNetwork['selected-regulons'] = networks[0]['selected-regulons'].map(selectedRegulon => {
                let matchedGenes = [];
                let matchedRegulatingTfs = [];
                let matchedTargetTfs = [];

                // check for genes
                selectedRegulon.targetGenes.forEach(gene => {
                    let found = true;
                    for (let i = 1; i < networks.length; i++) {
                        found = !!networks[i]['selected-regulons'].find(r => r.regulatorName && r.regulatorName.toLowerCase() === selectedRegulon.regulatorName.toLowerCase()).targetGenes.find(g => g.name && gene.name.toLowerCase() === g.name.toLowerCase());
                        if (!found) break;
                    }
                    if (!found) matchedGenes.push(gene);
                });
                for (let i = 1; i < networks.length; i++) {
                    let xorGenes = networks[i]['selected-regulons'].find(r => r.regulatorName && r.regulatorName.toLowerCase() === selectedRegulon.regulatorName.toLowerCase()).targetGenes.filter(g => g.name && !matchedGenes.map(mg => mg.name && mg.name.toLowerCase()).includes(g.name.toLowerCase()) && !networks[0]['selected-regulons'].find(r => r.regulatorName && r.regulatorName.toLowerCase() === selectedRegulon.regulatorName.toLowerCase()).targetGenes.map(tg => tg.name && tg.name.toLowerCase()).includes(g.name.toLowerCase()));
                    matchedGenes.push(...xorGenes);
                }

                // check for regulating regulons
                selectedRegulon.regulatingTFs.forEach(tf => {
                    let found = true;
                    for (let i = 1; i < networks.length; i++) {
                        found = !!networks[i]['selected-regulons'].find(r => r.regulatorName && r.regulatorName.toLowerCase() === selectedRegulon.regulatorName.toLowerCase()).regulatingTFs.find(r => r.name && tf.name.toLowerCase() === r.name.toLowerCase());
                        if (!found) break;
                    }
                    if (!found) matchedRegulatingTfs.push(tf);
                });
                for (let i = 1; i < networks.length; i++) {
                    let xorTFs = networks[i]['selected-regulons'].find(r => r.regulatorName && r.regulatorName.toLowerCase() === selectedRegulon.regulatorName.toLowerCase()).regulatingTFs.filter(tf => !matchedRegulatingTfs.map(mtf => mtf.name && mtf.name.toLowerCase()).includes(tf.name.toLowerCase()) && !networks[0]['selected-regulons'].find(r => r.regulatorName && r.regulatorName.toLowerCase() === selectedRegulon.regulatorName.toLowerCase()).regulatingTFs.map(ttf => ttf.name && ttf.name.toLowerCase()).includes(tf.name.toLowerCase()));
                    matchedRegulatingTfs.push(...xorTFs);
                }

                // check for target regulons
                selectedRegulon.targetTFs.forEach(tf => {
                    let found = true;
                    for (let i = 1; i < networks.length; i++) {
                        found = !!networks[i]['selected-regulons'].find(r => r.regulatorName && r.regulatorName.toLowerCase() === selectedRegulon.regulatorName.toLowerCase()).targetTFs.find(r => tf.name && tf.name.toLowerCase() === r.name.toLowerCase());
                        if (!found) break;
                    }
                    if (!found) matchedTargetTfs.push(tf);
                });
                for (let i = 1; i < networks.length; i++) {
                    let xorTFs = networks[i]['selected-regulons'].find(r => r.regulatorName && r.regulatorName.toLowerCase() === selectedRegulon.regulatorName.toLowerCase()).targetTFs.filter(tf => !matchedTargetTfs.map(mtf => mtf.name && mtf.name.toLowerCase()).includes(tf.name.toLowerCase()) && !networks[0]['selected-regulons'].find(r => r.regulatorName && r.regulatorName.toLowerCase() === selectedRegulon.regulatorName.toLowerCase()).targetTFs.map(ttf => ttf.name && ttf.name.toLowerCase()).includes(tf.name.toLowerCase()));
                    matchedRegulatingTfs.push(...xorTFs);
                }
                return {
                    regulatorName: selectedRegulon.regulatorName,
                    targetGenes: matchedGenes,
                    regulatingTFs: matchedRegulatingTfs,
                    targetTFs: matchedTargetTfs
                };
            });
        }
    }
    cb(null, binaryNetwork);
};
