const express = require('express');
const router = express.Router();

const regprecise = require('../libs/regprecise-local.js');
const binaryComparison = require('../libs/binary-comparison.js');
const regulondb = require('../libs/regulondb.js');

// const regNetwork = require('../libs/reg-network.js')

router.get('/status', (_req, res) => {
    regprecise.status((err, status) => {
        if (err) { console.error(err); return res.status(500).end(); }
        res.send(`${status}`);
    });
});

router.get('/genomes', (req, res) => {
    regprecise.genomes(_g => true, (err, genomes) => {
        if (err) { console.error(err); return res.status(500).end(); }
        if (req.headers['content-type'] === 'application/json') return res.json(genomes);
        res.render('regprecise/tables/rp-genomes.pug', { genomes: genomes });
    });
});

router.get('/regulogs', (req, res) => {
    regprecise.regulogs(generateFilter(req.query), (err, regulogs) => {
        if (err) { console.error(err); return res.status(500).end(); }
        res.json(regulogs);
    });
});

router.get('/regulons', (req, res) => {
    if (!Object.keys(req.query).length || !req.query.genomeId) return res.redirect('/regprecise/genomes');

    regprecise.genomes(g => g.genomeId === req.query.genomeId, (err, genome) => {
        if (err) { console.error(err); return res.status(500).end(); }

        regprecise.regulons(r => r.genomeId === req.query.genomeId, (err, regulons) => {
            if (err) { console.error(err); return res.status(500).end(); }

            regprecise.getTFNetwork(req.query.genomeId, (err, network) => {
                if (err) { console.error(err); return res.status(500).end(); }
                if (req.headers['content-type'] === 'application/json') return res.json(network);
                res.render('regprecise/tables/rp-regulons.pug', { genome: genome[0], regulons: regulons, network: network });
            });
        });
    });
});

router.get('/pathway', (req, res) => {
    if (!Object.keys(req.query).length || !req.query.regulons) return res.redirect('/regprecise/genomes');
    regprecise.getPathway(req.query.regulons, (err, network) => {
        if (err) { console.err(err); return res.status(500).end(); }
        regprecise.getPathwayGenomes(network, (err, network) => {
            if (err) { console.err(err); return res.status(500).end(); }
            if (req.headers['content-type'] === 'application/json') return res.json(network);
            res.render('regprecise/pathway.pug', { network: network });
        });
    });
});

router.get('/genes', (req, res) => {
    regprecise.genes(generateFilter(req.query), (err, genes) => {
        if (err) { console.error(err); return res.status(500).end(); }
        res.json(genes);
    });
});

router.get('/reguloggraph', (req, res) => {
    if (!Object.keys(req.query).length || !req.query.regulonId) return res.redirect('/regprecise/genomes');
    regprecise.getRegulogNetwork(req.query.regulonId, (err, network) => {
        if (err) { console.error(err); return res.status(500).end(); }
        if (req.headers['content-type'] === 'application/json') return res.json(network);
        res.render('regprecise/wagon-wheels.pug', { network: network });
    });
});

router.post('/kmeanscluster', (req, res) => {
    regprecise.kMeansCluster(req.body.data, req.body.k, (err, result) => {
        if (err) { console.error(err); return res.status(500).end(); }
        res.json(result);
    });
});

router.post('/binaryComparison', (req, res) => {
    regprecise.getPathways(req.body.data.regpreciseRegulons, (err, regpreciseNetworks) => {
        if (err) { console.error(err); return res.status(500).end(); }
        regulondb.getPathways(req.body.data.regulonDbRegulons, (err, regulonDbNetworks) => {
            if (err) { console.error(err); return res.status(500).end(); }
            binaryComparison.binaryComparison([...regulonDbNetworks, ...regpreciseNetworks], req.body.data.type, (err, comparison) => {
                if (err) { console.error(err); return res.sendStatus(500).end(); }
                return res.json(comparison);
            });
        })
    });
});

function generateFilter(reqQuery) {
    let keys = Object.keys(reqQuery);
    if (!keys.length) return true;
    return (obj) => {
        for (let key of keys) {
            if (obj[key] !== reqQuery[key]) return false;
        }
        return true;
    };
}

module.exports = router;
