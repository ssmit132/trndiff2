const express = require('express');
const router = express.Router();

const regulondb = require('../libs/regulondb.js');
const regNetwork = require('../libs/reg-network.js');
const regprecise = require('../libs/regprecise.js');
const binaryComparison = require('../libs/binary-comparison.js');

router.get('/regulators', (req, res) => {
    regulondb.getRegulators((err, network) => {
        if (err) { console.error(err); return res.status(500).end(); }
        res.json(req.query.graphable ? regNetwork.trNetworkToGraph(network) : network);
    });
});

/**
 * Returns the data for the regulon network in a JSON format. This includes
 * @param {}
 */
router.get('/regulonnetworkdata', (req, res) => {
    if (req.headers['content-type'] === 'application/json') {
        regulondb.getRegulators((err, regulators) => {
            if (err) { console.error(err); return res.status(500).end(); }
            regulondb.getRegulonNetwork(regulators, (err, data) => {
                if (err) {
                    console.error(err);
                    return res.status(500).end();
                }
                res.json(data);
            });
        });
    } else {
        return res.status(500).end();
    }
});

// router.get('/pathway', (req, res) => {
//     if (req.headers['content-type'] === 'application/json') {
//         regulondb.Genes((err, geneNetwork) => {
//             if (err) { console.error(err); return res.status(500).end(); }
//             regulondb.getGeneNetwork(selectedRegulons, geneNetwork, (err, network) => {
//                 if (err) {
//                     console.error(err);
//                     return res.status(500).end();
//                 }
//                 res.json(network);
//             });
//             res.json();
//         });
//     } else {
//         res.render('regulondb/regulon-network.pug');
//     }
// });

router.get('/pathway', (req, res) => {
    if (!Object.keys(req.query).length || !req.query.regulons) return res.redirect('/regulonnetwork');
    regulondb.getPathway(req.query.regulons, (err, network) => {
        if (err) { console.err(err); return res.status(500).end(); }
        if (req.headers['content-type'] === 'application/json') return res.json(network);
        res.render('regulondb/pathway.pug', { network: network });
    });
});

router.get('/regulonnetworkview', (_req, res) => {
    res.render('regulondb/regulon-network.pug');
});

router.post('/binaryComparison', (req, res) => {
    regprecise.getPathways(req.body.data.regpreciseRegulons, (err, regpreciseNetworks) => {
        if (err) { console.error(err); return res.status(500).end(); }
        regulondb.getPathways(req.body.data.regulonDbRegulons, (err, regulonDbNetworks) => {
            if (err) { console.error(err); return res.status(500).end(); }
            binaryComparison.binaryComparison([...regpreciseNetworks, ...regulonDbNetworks], req.body.data.type, (err, comparison) => {
                if (err) { console.error(err); return res.sendStatus(500).end(); }
                return res.json(comparison);
            });
        })
    });
});
module.exports = router;
