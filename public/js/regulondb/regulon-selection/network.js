import * as graphFunctions from '../../regulon-selection-graph.js';
import * as globalFunctions from '../../global-functions.js';

const API_URL = '/regulondb';
$(document).ready(() => {
    let tooltip;

    $('body').append(tooltip = $('<tooltip>')
        .addClass('ui segment')
        .css('position', 'absolute')
        .css('z-index', 1000)
        .css('visibility', 'hidden')
        .css('padding', '10px')
        .css('background-color', 'rgba(255,255,255,0.75)'));

    let graphDimmerEl = $('#directed-graph #body #dimmer');
    graphDimmerEl.dimmer('show');
    let clearButtonEl = $('#showModalButton');
    let tableDimmerEl = $('#table #dimmer');
    tableDimmerEl.dimmer('show');

    let graphEl = $('#directed-graph #body #graph');
    graphEl.height(Math.max(500, $(document).height() - graphEl.offset().top - 15));

    $.ajax({
        tpye: 'GET',
        url: `${API_URL}/regulonnetworkdata`,
        contentType: 'application/json',
        success: function(response) {
            let graph, data;
            updateEffects(response.effects);
            updateCodes(response.evidenceCodes);

            [graph, data] = graphFunctions.createGraph(response.network, graphEl, graphDimmerEl, tooltip, API_URL, clearButtonEl);
            createTable(response.network, data);
            setUserInteraction(response.network, data, graph);
            importView();

            displayGoTermFilters(response.goTerms);
        }
    });
});

function updateCodes(codes) {
    let codesEls = [];
    codes.forEach(code => {
        codesEls.push(
            $('<option>').val(code).text(code)
        );
    });
    $('#evidence-values').append(...codesEls);
}

function updateEffects(effects) {
    let effectsEls = [];
    effects.forEach(effect => {
        effectsEls.push(
            $('<option>').val(effect).text(effect)
        );
    });
    $('#regulatory-effect').append(...effectsEls);
}

function createTable(regulonNetwork, visData) {
    let clearButtonEl = $('#showModalButton');

    regulonNetwork.forEach(regulator => {
        $('#regulonTable tbody').append(
            $('<tr>').append(
                $('<td>').text(regulator.regulatorName)
            ).append(
                $('<td>').text(regulator.effects.join(', '))
            ).append(
                $('<td>').text(regulator.evidenceCodes.join(', '))
            ).append(
                $('<td>').text(regulator.strengths.join(', '))
            ).attr('id', `${regulator.regulatorName.toLowerCase()}`)
        );
    });

    $('#regulonTable tbody tr').click((e) => {
        if (e && e.currentTarget && e.currentTarget.id) {
            const regulonName = regulonNetwork.find(r => r.regulatorName.toLowerCase().replace(/[|&;$%@"<>()+,\\\/]/g, '') === e.currentTarget.id).regulatorName.replace(/[|&;$%@"<>()+,\\\/]/g, '');
            graphFunctions.selectNode(regulonName, regulonNetwork, visData, API_URL, clearButtonEl);
        }
    });
}

/**
 *
 * @param {boolean} show The local variable storing the show toggle option for the entered element
 * @param {boolean} hide  The hide variable storing the hide toggle option for the entered element
 * @param {boolean} only The show only variable storing the show only toggle option for the entered element
 * @param {Element} showEl The element representing the entered show button
 * @param {Element} hideEl The element representing the entered hide button
 * @param {Element} onlyEl The element representing the entered show only button
 */
function updateButtonStyle(show, hide, only, showEl, hideEl, onlyEl) {
    if (show) {
        showEl.addClass('blue');
        hideEl.removeClass('blue');
        onlyEl.removeClass('blue');
    } else if (hide) {
        showEl.removeClass('blue');
        hideEl.addClass('blue');
        onlyEl.removeClass('blue');
    } else if (only) {
        showEl.removeClass('blue');
        hideEl.removeClass('blue');
        onlyEl.addClass('blue');
    } else {
        showEl.removeClass('blue');
        hideEl.removeClass('blue');
        onlyEl.removeClass('blue');
    }
}

function setUserInteraction(network, visGraphData, visGraph) {
    // Set variables representing the button elements
    let selfRegulatedShow = $('#directed-graph #header #group-self-regulated #show');
    let selfRegulatedHide = $('#directed-graph #header #group-self-regulated #hide');
    let selfRegulatedOnly = $('#directed-graph #header #group-self-regulated #only');
    let unregulatedShow = $('#directed-graph #header #group-hide-unregulated #show');
    let unregulatedHide = $('#directed-graph #header #group-hide-unregulated #hide');
    let unregulatedOnly = $('#directed-graph #header #group-hide-unregulated #only');

    let clearButtonEl = $('#showModalButton');
    let tfNameSearchEl = $('#directed-graph #header #search-node #regulator-search');
    let geneNameSearchEl = $('#directed-graph #header #search-node #target-search');

    $('#showModalButton').click(() => {
        $('#clearModal').modal('show');
    });

    $('#clearModal #clearButton').click(() => {
        graphFunctions.clearSelectedNodes(visGraphData, clearButtonEl);
    });

    tfNameSearchEl.bind('input', () => {
        completeSearch(network, visGraph);
    });

    geneNameSearchEl.bind('input', () => {
        completeSearch(network, visGraph);
    });

    $('#directed-graph #header #search-node #remove-highlight').click(() => {
        geneNameSearchEl.val(undefined);
        tfNameSearchEl.val(undefined);
        completeSearch(network, visGraph);
    });

    $('#directed-graph #header #toggle-arrows.checkbox').checkbox({
        onChange: function() {
            graphFunctions.toggleEdgeArrows(this.checked, visGraphData);
        }
    });

    $('#regulonTable tbody tr').click((e) => {
        if (e && e.currentTarget && e.currentTarget.id) {
            completeSearch(network, visGraph);
        }
    });

    $('#directed-graph #header #regulatory-effect').dropdown({
        onChange: () => {
            completeSearch(network, visGraph);
        }
    });

    $('#directed-graph #header #evidence-values').dropdown({
        onChange: () => {
            completeSearch(network, visGraph);
        }
    });

    // Show unregulated tfs when show unregulated button is clicked
    unregulatedShow.click(function() {
        // Update styling for the button
        updateButtonStyle(true, false, false, unregulatedShow, unregulatedHide, unregulatedOnly);

        // If user wants to show unregulated tfs but show only self-regulated is toggled, set self-regulated filter to show
        if (selfRegulatedOnly.hasClass('blue')) {
            updateButtonStyle(true, false, false, selfRegulatedShow, selfRegulatedHide, selfRegulatedOnly);
        }

        // Apply filters to graph
        completeSearch(network, visGraph);
    });

    // Hide the unregulated tfs when hide regulated button is clicked
    unregulatedHide.click(function() {
        // Update styling for the button
        updateButtonStyle(false, true, false, unregulatedShow, unregulatedHide, unregulatedOnly);

        // Apply filters to graph
        completeSearch(network, visGraph);
    });

    // Show only unregulated tfs when show only unregulated button is clicked
    unregulatedOnly.click(function() {
        // Update styling for the buttons. Set unregulated to show only and self-regulated to hide
        updateButtonStyle(false, false, true, unregulatedShow, unregulatedHide, unregulatedOnly);
        updateButtonStyle(false, true, false, selfRegulatedShow, selfRegulatedHide, selfRegulatedOnly);

        if (selfRegulatedOnly.hasClass('blue') && unregulatedOnly.hasClass('blue')) {
            updateButtonStyle(false, false, false, selfRegulatedShow, selfRegulatedHide, selfRegulatedOnly);
            updateButtonStyle(false, false, false, unregulatedShow, unregulatedHide, unregulatedOnly);
            $('#invalidModal').modal('show');
        }

        // Apply filters to graph
        completeSearch(network, visGraph);
    });

    // Show self-regulated tfs when show self-regulated button is clicked
    selfRegulatedShow.click(function() {
        // Update styling for the button
        updateButtonStyle(true, false, false, selfRegulatedShow, selfRegulatedHide, selfRegulatedOnly);

        // If user wants to show self-regulated tfs but show only unregulated is toggled, set unregulated filter to show
        if (unregulatedOnly.hasClass('blue')) {
            updateButtonStyle(true, false, false, unregulatedShow, unregulatedHide, unregulatedOnly);
        }

        // Apply filters to graph
        completeSearch(network, visGraph);
    });

    // Hide self-regulated tfs when hide self-regulated button is clicked
    selfRegulatedHide.click(function() {
        // Update styling for the button
        updateButtonStyle(false, true, false, selfRegulatedShow, selfRegulatedHide, selfRegulatedOnly);

        // Apply filters to graph
        completeSearch(network, visGraph);
    });

    // Show only self-regulated tfs when show only self-regulated button is clicked
    selfRegulatedOnly.click(function() {
        // Update styling for the buttons. Set self-regulated to show only and unregulated to hide
        updateButtonStyle(false, false, true, selfRegulatedShow, selfRegulatedHide, selfRegulatedOnly);
        updateButtonStyle(false, true, false, unregulatedShow, unregulatedHide, unregulatedOnly);

        if (selfRegulatedOnly.hasClass('blue') && unregulatedOnly.hasClass('blue')) {
            updateButtonStyle(false, false, false, selfRegulatedShow, selfRegulatedHide, selfRegulatedOnly);
            updateButtonStyle(false, false, false, unregulatedShow, unregulatedHide, unregulatedOnly);
            $('#invalidModal').modal('show');
        }

        // Apply filters to graph
        completeSearch(network, visGraph);
    });

    const goTermEls = $('#goterm-legend .buttons #search');
    goTermEls.click(() => {
        completeSearch(network, visGraph);
    });

    const goTermClearEls = $('#goterm-legend .buttons #clear');
    goTermClearEls.click(() => {
        graphFunctions.applyFilters($('#directed-graph #header #evidence-values').val(), $('#directed-graph #header #regulatory-effect').val(), network, visGraph, geneNameSearchEl.val(), tfNameSearchEl.val(), $('#directed-graph #header #group-self-regulated').find('.blue').attr('id'), $('#directed-graph #header #group-hide-unregulated').find('.blue').attr('id'), undefined);
    });
    applyFilters(network, visGraph);
}

function displayGoTermFilters(goTerms) {
    goTerms && goTerms.forEach(term => {
        $(`#goterm-legend #${term.replace(/\s/g, '')}`).show();
    });
}

function getActiveGoTerm() {
    const goEl = $('#goterm-legend .buttons').has('#clear:visible');
    return goEl && goEl.attr('id');
}

function completeSearch(network, visGraph) {
    applyFilters(network, visGraph);
    createUrl();
}

function applyFilters(network, visGraph) {
    const evidenceValues = $('#directed-graph #header #evidence-values').val();
    const effects = $('#directed-graph #header #regulatory-effect').val();
    const regulatorVal = $('#directed-graph #header #search-node #regulator-search').val();
    const targetVal = $('#directed-graph #header #search-node #target-search').val();
    const selfRegulatedState = $('#directed-graph #header #group-self-regulated').find('.blue').attr('id');
    const unregulatedState = $('#directed-graph #header #group-hide-unregulated').find('.blue').attr('id');
    graphFunctions.applyFilters(evidenceValues, effects, network, visGraph, targetVal, regulatorVal, selfRegulatedState, unregulatedState, getActiveGoTerm());
}

function createUrl() {
    globalFunctions.createUrl();
}

function importView() {
    const query = window.location.search;
    const params = new URLSearchParams(query);
    if (params.has('effects')) {
        const effects = params.get('effects').split(' ');
        $('#regulatory-effect').dropdown('set selected', effects);
    }

    if (params.has('evidencecodes')) {
        const codes = params.get('evidencecodes').split(' ');
        $('#evidence-values').dropdown('set selected', codes);
    }
    createUrl();

    $('#export-view-button').click(() => {
        globalFunctions.exportView();
    });
}
