import * as graphFunctions from '../../pathway-analysis-graph.js';
import * as globalFunctions from '../../global-functions.js';
const REGPRECIE_API_URL = '/regprecise/';
const REGULONDB_API_URL = '/regulondb/';

$(document).ready(() => {
    let tooltip = $('<tooltip>').attr('id', 'tooltip');
    $('body').append(tooltip
        .addClass('ui segment')
        .css('position', 'absolute')
        .css('z-index', 1000)
        .css('visibility', 'hidden')
        .css('padding', '10px')
        .css('background-color', 'rgba(255,255,255,0.75)'));

    setMainGraph(tooltip);
    $(`#regulonDbGraph`).click(function(e) {
        selectGenomeCompare('regulonDbGraph', e);
    });

    $('#pathway-graph #header #number-of-genomes #genomes-selected').dropdown({
        onAdd: function(addedValue) {
            addNewGenome(addedValue);
            createUrl();
        },
        onRemove: function(removedValue) {
            $.when($(`#alternate-genomes #${removedValue}.genomeSection`).remove()).then(() => {
                createUrl();
            });
        }
    });
    importState();

    $('#btn-compare-and').click(() => { binaryComparisonGenome('AND'); });
    $('#btn-compare-or').click(() => { binaryComparisonGenome('OR'); });
    $('#btn-compare-xor').click(() => { binaryComparisonGenome('XOR'); });
});

/**
 * Set the main graph for the pathway
 * @param {Element} tooltip The element representing the tooltip
 */
function setMainGraph(tooltip) {
    $(`#${network['selected-regulons'][0].genomeId}.genome #dimmer`).dimmer('show');
    let graphEl = $('#pathway-graph #body');
    graphEl.height(Math.max(500, $(document).height() - graphEl.offset().top - 15));
    let [data, graph] = graphFunctions.createGraph(network, graphEl, tooltip, `#${network['selected-regulons'][0].genomeId}.genome #dimmer`, REGULONDB_API_URL, network['selected-regulons'][0].genomeId);
    setUserInteraction(data, graph);
    displayGoTermFilters(network);
}

/**
 * Add a new genome to the page and create its graph
 * @param {string} genomeId The genome id to be added
 */
function addNewGenome(genomeId) {
    let genome = network['repgrecise-genomes'].find(g => g.genomeId === genomeId);
    $('#alternate-genomes').append($('<div>').attr('id', genomeId).click(function(e) { selectGenomeCompare(genomeId, e); }).addClass('genome genomeSection ui card')
        .append($('<div>').addClass('ui basic segment')
        ).append($('<div>').attr('id', 'graph').addClass('genomeGraph')
        ).append($('<div>').addClass('ui centered small header').text(genome.name))
        .append($('<div>').addClass('ui inverted dimmer').attr('id', 'dimmer').append($('<div>').addClass('ui indeterminate large text loader').text('Loading Graph'))));

    let graphEl = $(`#alternate-genomes #${genomeId}.genomeSection #graph`);
    let tooltip = $('#tooltip');
    $(`#alternate-genomes #${genomeId}.genome #dimmer`).dimmer('show');
    $.ajax({
        tpye: 'GET',
        url: `${REGPRECIE_API_URL}pathway?regulons=${genome.regulonIds.join('+')}`,
        contentType: 'application/json',
        success: function(pathway) {
            let [data, graph] = graphFunctions.createGraph(pathway, graphEl, tooltip, `#alternate-genomes #${genomeId}.genome #dimmer`, REGPRECIE_API_URL, genomeId);
            setUserInteraction(data, graph);
            displayGoTermFilters(pathway);
            createUrl();
        }
    });
}

/**
 *
 * @param {boolean} show The local variable storing the show toggle option for the entered element
 * @param {boolean} hide  The hide variable storing the hide toggle option for the entered element
 * @param {boolean} only The show only variable storing the show only toggle option for the entered element
 * @param {Element} showEl The element representing the entered show button
 * @param {Element} hideEl The element representing the entered hide button
 * @param {Element} onlyEl The element representing the entered show only button
 */
function updateButtonStyle(show, hide, only, showEl, hideEl, onlyEl) {
    if (show) {
        showEl.addClass('blue');
        hideEl.removeClass('blue');
        onlyEl.removeClass('blue');
    } else if (hide) {
        showEl.removeClass('blue');
        hideEl.addClass('blue');
        onlyEl.removeClass('blue');
    } else if (only) {
        showEl.removeClass('blue');
        hideEl.removeClass('blue');
        onlyEl.addClass('blue');
    } else {
        showEl.removeClass('blue');
        hideEl.removeClass('blue');
        onlyEl.removeClass('blue');
    }
}

/**
 * Add user interaction functionality such as filters and graph manipulation
 * @param {VisDataSet} visGraphData The vis data set to be updated.
 * @param {visGraph} visGraph The vis network containing the graph.
 */
function setUserInteraction(visGraphData, visGraph) {
    // Set variables representing the button elements
    let selfRegulatedShow = $('#header #group-self-regulated #show');
    let selfRegulatedHide = $('#header #group-self-regulated #hide');
    let selfRegulatedOnly = $('#header #group-self-regulated #only');
    let unregulatedShow = $('#header #group-hide-unregulated #show');
    let unregulatedHide = $('#header #group-hide-unregulated #hide');
    let unregulatedOnly = $('#header #group-hide-unregulated #only');
    let tfNameSearchEl = $('#header #search-node #regulator-search');
    let geneNameSearchEl = $('#header #search-node #target-search');
    let clearSearchSel = $('#header #search-node #remove-highlight');

    tfNameSearchEl.bind('input', () => {
        completeSearch(visGraph);
    });

    geneNameSearchEl.bind('input', () => {
        completeSearch(visGraph);
    });

    clearSearchSel.click(() => {
        geneNameSearchEl.val(undefined);
        tfNameSearchEl.val(undefined);
        completeSearch(visGraph);
    });

    $('#remove-pathways').change(() => {
        completeSearch(visGraph);
    });

    $('#toggle-arrows.checkbox').change(() => {
        graphFunctions.toggleEdgeArrows($('#toggle-arrows.checkbox').checkbox('is checked'), visGraphData);
        createUrl();
    });

    $('#header #regulatory-effect').dropdown({
        onChange: () => {
            completeSearch(visGraph);
        }
    });

    $('#header #evidence-values').dropdown({
        onChange: () => {
            completeSearch(visGraph);
        }
    });

    // Show unregulated tfs when show unregulated button is clicked
    unregulatedShow.click(function() {
        // Update styling for the button
        updateButtonStyle(true, false, false, unregulatedShow, unregulatedHide, unregulatedOnly);

        // If user wants to show unregulated tfs but show only self-regulated is toggled, set self-regulated filter to show
        if (selfRegulatedOnly.hasClass('blue')) {
            updateButtonStyle(true, false, false, selfRegulatedShow, selfRegulatedHide, selfRegulatedOnly);
        }

        // Apply filters to graph
        completeSearch(visGraph);
    });

    // Hide the unregulated tfs when hide regulated button is clicked
    unregulatedHide.click(function() {
        // Update styling for the button
        updateButtonStyle(false, true, false, unregulatedShow, unregulatedHide, unregulatedOnly);

        // Apply filters to graph
        completeSearch(visGraph);
    });

    // Show only unregulated tfs when show only unregulated button is clicked
    unregulatedOnly.click(function() {
        // Update styling for the buttons. Set unregulated to show only and self-regulated to hide
        updateButtonStyle(false, false, true, unregulatedShow, unregulatedHide, unregulatedOnly);
        updateButtonStyle(false, true, false, selfRegulatedShow, selfRegulatedHide, selfRegulatedOnly);

        if (selfRegulatedOnly.hasClass('blue') && unregulatedOnly.hasClass('blue')) {
            updateButtonStyle(false, false, false, selfRegulatedShow, selfRegulatedHide, selfRegulatedOnly);
            updateButtonStyle(false, false, false, unregulatedShow, unregulatedHide, unregulatedOnly);
            $('#invalidModal').modal('show');
        }

        // Apply filters to graph
        completeSearch(visGraph);
    });

    // Show self-regulated tfs when show self-regulated button is clicked
    selfRegulatedShow.click(function() {
        // Update styling for the button
        updateButtonStyle(true, false, false, selfRegulatedShow, selfRegulatedHide, selfRegulatedOnly);

        // If user wants to show self-regulated tfs but show only unregulated is toggled, set unregulated filter to show
        if (unregulatedOnly.hasClass('blue')) {
            updateButtonStyle(true, false, false, unregulatedShow, unregulatedHide, unregulatedOnly);
        }

        // Apply filters to graph
        completeSearch(visGraph);
    });

    // Hide self-regulated tfs when hide self-regulated button is clicked
    selfRegulatedHide.click(function() {
        // Update styling for the button
        updateButtonStyle(false, true, false, selfRegulatedShow, selfRegulatedHide, selfRegulatedOnly);

        // Apply filters to graph
        completeSearch(visGraph);
    });

    // Show only self-regulated tfs when show only self-regulated button is clicked
    selfRegulatedOnly.click(function() {
        // Update styling for the buttons. Set self-regulated to show only and unregulated to hide
        updateButtonStyle(false, false, true, selfRegulatedShow, selfRegulatedHide, selfRegulatedOnly);
        updateButtonStyle(false, true, false, unregulatedShow, unregulatedHide, unregulatedOnly);

        if (selfRegulatedOnly.hasClass('blue') && unregulatedOnly.hasClass('blue')) {
            updateButtonStyle(false, false, false, selfRegulatedShow, selfRegulatedHide, selfRegulatedOnly);
            updateButtonStyle(false, false, false, unregulatedShow, unregulatedHide, unregulatedOnly);
            $('#invalidModal').modal('show');
        }

        // Apply filters to graph
        completeSearch(visGraph);
    });

    const goTermEls = $('#goterm-legend .buttons #search');
    goTermEls.click(() => {
        completeSearch(visGraph);
    });

    const goTermClearEls = $('#goterm-legend .buttons #clear');
    goTermClearEls.click(() => {
        graphFunctions.applyFilters($('#header #evidence-values').val(), $('#header #regulatory-effect').val(), visGraph, geneNameSearchEl.val(), tfNameSearchEl.val(), $('#header #group-self-regulated').find('.blue').attr('id'), $('#header #group-hide-unregulated').find('.blue').attr('id'), undefined);
    });
    graphFunctions.toggleEdgeArrows($('#toggle-arrows.checkbox').checkbox('is checked'), visGraphData);
    applyFilters(visGraph);
}

/**
 * When a graph is clicked select it and display a green check mark
 * @param {string} genomeId The genome id selected 
 */
function selectGenomeCompare(genomeId) {
    let div = $(`#${genomeId}.genome`);
    if (div.children('.green.label').length > 0) {
        div.children('.green.label').remove();
    } else {
        div.append(
            $('<div>').addClass('ui right corner green label').append(
                $('<i>').addClass('check icon')
            )
        );
    }
    if ($('.genome').has('.green.label').length > 1) {
        $('#pathway-graph #header #compare-dropdown').dropdown().show();
    } else {
        $('#pathway-graph #header #compare-dropdown').dropdown().hide();
    };
}

/**
 * Perform a binary comparison on selected graph
 * @param {string} comparisonType The binary operation to perform on the graphs
 */
function binaryComparisonGenome(comparisonType) {
    let genomes = $('.genome').has('.green.label');
    let data = {
        type: comparisonType,
        regpreciseRegulons: [],
        regulonDbRegulons: []
    };
    for (let genomeEl of genomes) {
        let genome = network['repgrecise-genomes'].find(g => g.genomeId === genomeEl.id);
        if (genome) {
            data.regpreciseRegulons.push(genome.regulonIds);
        } else {
            data.regulonDbRegulons.push(network['selected-regulons'].map(r => r.regulatorName.toLowerCase()));
        }
    }
    $('#comparison-graph').empty();
    $('#comparison-modal .header #type').text(comparisonType.toUpperCase());
    $('#comparison-modal').modal('show');
    $('#comparison-modal #comparison-dimmer').dimmer('show');
    $.ajax({
        type: 'POST',
        data: JSON.stringify({ data }),
        dataType: 'json',
        url: `${REGPRECIE_API_URL}binaryComparison`,
        contentType: 'application/json',
        success: function(comparisonNetwork) {
            graphFunctions.createComparisonGraph(comparisonNetwork);
            // find the graph in the same sort of way as all others
            $('#comparison-modal #comparison-dimmer').dimmer('hide');
        }
    });
}

/**
 * Display the go term filters
 * @param {visDataSet} pathway The data set of the pathway 
 */
function displayGoTermFilters(pathway) {
    pathway.goTerms && pathway.goTerms.forEach(term => {
        $(`#goterm-legend #${term.replace(/\s/g, '')}`).show();
    });
}

/**
 * Get the active go terms for filtering
 */
function getActiveGoTerm() {
    const goEl = $('#goterm-legend .buttons').has('#clear:visible');
    return goEl && goEl.attr('id');
}

function completeSearch(visGraph) {
    applyFilters(visGraph);
    createUrl();
}

/**
 * Create the URL which is the combination of genome ID's
 */
function createUrl() {
    // Get the specific filter options
    const selectedIDs = network['selected-regulons'].map(r => r.regulatorName.toLowerCase());
    // Get the alternate genomes that are displayed
    const options = {
        regulons: selectedIDs.join('+')
    };
    globalFunctions.createUrl(options);
}

/**
 * Get the elements for the filters and apply them to the new graph
 * @param {visGraph} visGraph The vis netwokr graph
 */
function applyFilters(visGraph) {
    const tfNameSearchEl = $('#header #search-node #regulator-search');
    const geneNameSearchEl = $('#header #search-node #target-search');
    const selfRegulatedState = $('#header #group-self-regulated').find('.blue').attr('id');
    const unregulatedState = $('#header #group-hide-unregulated').find('.blue').attr('id');
    const displayEdges = $('#pathway-graph #header #remove-pathways.checkbox').checkbox('is checked');
    graphFunctions.applyFilters($('#header #evidence-values').val(), $('#header #regulatory-effect').val(), visGraph, geneNameSearchEl.val(), tfNameSearchEl.val(), selfRegulatedState, unregulatedState, getActiveGoTerm(), displayEdges);
}

/**
 * Import the state of the program to revert to saved changes
 */
function importState() {
    const query = window.location.search;
    const params = new URLSearchParams(query);

    if (params.has('alternategenomes')) {
        const genomes = params.get('alternategenomes').split(' ');
        $('#genomes-selected').dropdown('set exactly', genomes);
    }

    if (params.has('effects')) {
        const effects = params.get('effects').split(' ');
        $('#regulatory-effect').dropdown('set selected', effects);
    }

    if (params.has('evidencecodes')) {
        const codes = params.get('evidencecodes').split(' ');
        $('#evidence-values').dropdown('set selected', codes);
    }
    $('#export-view-button').click(() => {
        globalFunctions.exportView();
    });
    createUrl();
}
