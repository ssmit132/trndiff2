import * as graphFunctions from '../../regulon-selection-graph.js';
import * as globalFunctions from '../../global-functions.js';

const API_URL = '/regprecise';
$(document).ready(() => {
    let tooltip;

    $('body').append(tooltip = $('<tooltip>')
        .addClass('ui segment')
        .css('position', 'absolute')
        .css('z-index', 1000)
        .css('visibility', 'hidden')
        .css('padding', '10px')
        .css('background-color', 'rgba(255,255,255,0.75)'));

    let graphEl = $('#directed-graph #body #graph');
    let graphDimmerEl = $('#directed-graph #body #dimmer');
    let clearButtonEl = $('#showModalButton');

    graphDimmerEl.dimmer('show');
    graphEl.height(Math.max(500, $(document).height() - graphEl.offset().top - 15));
    let [graph, data] = graphFunctions.createGraph(network.network, graphEl, graphDimmerEl, tooltip, API_URL, clearButtonEl);
    displayGoTermFilters(network.goTerms);
    setUserInteraction(network.network, graph, data);
    $('#export-view-button').click(() => {
        globalFunctions.exportView();
    });
});

/**
 *
 * @param {boolean} show The local variable storing the show toggle option for the entered element
 * @param {boolean} hide  The hide variable storing the hide toggle option for the entered element
 * @param {boolean} only The show only variable storing the show only toggle option for the entered element
 * @param {jquery element} showEl The element representing the entered show button
 * @param {jquery element} hideEl The element representing the entered hide button
 * @param {jquery element} onlyEl The element representing the entered show only button
 */
function updateButtonStyle(show, hide, only, showEl, hideEl, onlyEl) {
    if (show) {
        showEl.addClass('blue');
        hideEl.removeClass('blue');
        onlyEl.removeClass('blue');
    } else if (hide) {
        showEl.removeClass('blue');
        hideEl.addClass('blue');
        onlyEl.removeClass('blue');
    } else if (only) {
        showEl.removeClass('blue');
        hideEl.removeClass('blue');
        onlyEl.addClass('blue');
    } else {
        showEl.removeClass('blue');
        hideEl.removeClass('blue');
        onlyEl.removeClass('blue');
    }
}
/**
 * Add user interaction functionality such as filters and graph manipulation
 * @param {network} visGraphData The network of the grpah
 * @param {visGraph} visGraph The vis network containing the graph.
 * @param {visDataSet} The vis data set to be updated.
 */
function setUserInteraction(network, graph, data) {
    // Set variables representing the button elements
    let selfRegulatedShow = $('#directed-graph #header #group-self-regulated #show');
    let selfRegulatedHide = $('#directed-graph #header #group-self-regulated #hide');
    let selfRegulatedOnly = $('#directed-graph #header #group-self-regulated #only');
    let unregulatedShow = $('#directed-graph #header #group-hide-unregulated #show');
    let unregulatedHide = $('#directed-graph #header #group-hide-unregulated #hide');
    let unregulatedOnly = $('#directed-graph #header #group-hide-unregulated #only');

    let graphDimmerEl = $('#directed-graph #body #dimmer');
    let tfNameSearchEl = $('#directed-graph #header #search-node #regulator-search');
    let geneNameSearchEl = $('#directed-graph #header #search-node #target-search');
    let clearButtonEl = $('#showModalButton');

    $('#showModalButton').click(() => {
        $('#clearModal').modal('show');
    });

    tfNameSearchEl.bind('input', function() {
        completeSearch(network, graph);
    });

    geneNameSearchEl.bind('input', function() {
        completeSearch(network, graph);
    });

    $('#directed-graph #header #search-node #remove-highlight').click(function() {
        geneNameSearchEl.val(undefined);
        tfNameSearchEl.val(undefined);
        completeSearch(network, graph);
    });

    $('#directed-graph #header #toggle-arrows.checkbox').checkbox({
        onChange: function() { graphFunctions.toggleEdgeArrows(this.checked, data); }
    });

    $('#clearModal #clearButton').click(() => {
        graphFunctions.clearSelectedNodes(data, clearButtonEl);
    });

    $('#regulonTable tbody tr').click((e) => {
        if (e && e.currentTarget && e.currentTarget.id) {
            const regulonName = network.find(r => r.regulatorName.toLowerCase().replace(/[|&;$%@"<>()+,\\\/]/g, '') === e.currentTarget.id).regulatorName.replace(/[|&;$%@"<>()+,\\\/]/g, '');
            graphFunctions.selectNode(regulonName, network, data, API_URL, clearButtonEl);
        }
    });

    // Show unregulated tfs when show unregulated button is clicked
    unregulatedShow.click(function() {
        // Update styling for the button
        updateButtonStyle(true, false, false, unregulatedShow, unregulatedHide, unregulatedOnly);

        // If user wants to show unregulated tfs but show only self-regulated is toggled, set self-regulated filter to show
        if (selfRegulatedOnly.hasClass('blue')) {
            updateButtonStyle(true, false, false, selfRegulatedShow, selfRegulatedHide, selfRegulatedOnly);
        }

        // Apply filters to graph
        completeSearch(network, graph);
    });

    // Hide the unregulated tfs when hide regulated button is clicked
    unregulatedHide.click(function() {
        // Update styling for the button
        updateButtonStyle(false, true, false, unregulatedShow, unregulatedHide, unregulatedOnly);

        // Apply filters to graph
        completeSearch(network, graph);
    });

    // Show only unregulated tfs when show only unregulated button is clicked
    unregulatedOnly.click(function() {
        // Update styling for the buttons. Set unregulated to show only and self-regulated to hide
        updateButtonStyle(false, false, true, unregulatedShow, unregulatedHide, unregulatedOnly);
        updateButtonStyle(false, true, false, selfRegulatedShow, selfRegulatedHide, selfRegulatedOnly);

        if (selfRegulatedOnly.hasClass('blue') && unregulatedOnly.hasClass('blue')) {
            updateButtonStyle(false, false, false, selfRegulatedShow, selfRegulatedHide, selfRegulatedOnly);
            updateButtonStyle(false, false, false, unregulatedShow, unregulatedHide, unregulatedOnly);
            $('#invalidModal').modal('show');
        }

        // Apply filters to graph
        completeSearch(network, graph);
    });

    // Show self-regulated tfs when show self-regulated button is clicked
    selfRegulatedShow.click(function() {
        // Update styling for the button
        updateButtonStyle(true, false, false, selfRegulatedShow, selfRegulatedHide, selfRegulatedOnly);

        // If user wants to show self-regulated tfs but show only unregulated is toggled, set unregulated filter to show
        if (unregulatedOnly.hasClass('blue')) {
            updateButtonStyle(true, false, false, unregulatedShow, unregulatedHide, unregulatedOnly);
        }

        // Apply filters to graph
        completeSearch(network, graph);
    });

    // Hide self-regulated tfs when hide self-regulated button is clicked
    selfRegulatedHide.click(function() {
        // Update styling for the button
        updateButtonStyle(false, true, false, selfRegulatedShow, selfRegulatedHide, selfRegulatedOnly);

        // Apply filters to graph
        completeSearch(network, graph);
    });

    // Show only self-regulated tfs when show only self-regulated button is clicked
    selfRegulatedOnly.click(function() {
        // Update styling for the buttons. Set self-regulated to show only and unregulated to hide
        updateButtonStyle(false, false, true, selfRegulatedShow, selfRegulatedHide, selfRegulatedOnly);
        updateButtonStyle(false, true, false, unregulatedShow, unregulatedHide, unregulatedOnly);

        if (selfRegulatedOnly.hasClass('blue') && unregulatedOnly.hasClass('blue')) {
            updateButtonStyle(false, false, false, selfRegulatedShow, selfRegulatedHide, selfRegulatedOnly);
            updateButtonStyle(false, false, false, unregulatedShow, unregulatedHide, unregulatedOnly);
            $('#invalidModal').modal('show');
        }

        // Apply filters to graph
        completeSearch(network, graph);
    });

    const goTermEls = $('#goterm-legend .buttons #search');
    goTermEls.click(() => {
        completeSearch(network, graph);
    });
    const goTermClearEls = $('#goterm-legend .buttons #clear');
    goTermClearEls.click(() => {
        graphFunctions.applyFilters(undefined, undefined, network, graph, geneNameSearchEl.val(), tfNameSearchEl.val(), $('#directed-graph #header #group-self-regulated').find('.blue').attr('id'), $('#directed-graph #header #group-hide-unregulated').find('.blue').attr('id'), undefined);
    });
    completeSearch(network, graph);
}

/**
 * Display the go term filters
 * @param {string[]} pathway The go terms for data in the graph 
 */
function displayGoTermFilters(goTerms) {
    goTerms && goTerms.forEach(term => {
        $(`#goterm-legend #${term.replace(/\s/g, '')}`).show();
    });
}

/**
 * Set the active go term
 */
function getActiveGoTerm() {
    const goEl = $('#goterm-legend .buttons').has('#clear:visible');
    return goEl && goEl.attr('id');
}

function completeSearch(network, graph) {
    applyFilters(network, graph);
    createUrl();
}

/**
 * Get the elements for the filters and apply them to the new graph
 * @param {network} network The network of the graph
 * @param {visGraph} visGraph The vis network graph
 */
function applyFilters(network, graph) {
    const regulatorVal = $('#directed-graph #header #search-node #regulator-search').val();
    const targetVal = $('#directed-graph #header #search-node #target-search').val();
    const selfRegulatedState = $('#directed-graph #header #group-self-regulated').find('.blue').attr('id');
    const unregulatedState = $('#directed-graph #header #group-hide-unregulated').find('.blue').attr('id');
    graphFunctions.applyFilters(undefined, undefined, network, graph, targetVal, regulatorVal, selfRegulatedState, unregulatedState, getActiveGoTerm());
}

function createUrl() {
    const options = {
        genomeId: network.network[0].genomeId
    };

    globalFunctions.createUrl(options);
}
