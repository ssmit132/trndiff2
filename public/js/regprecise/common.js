$(document).ready(() => {
    $('.dimmer').dimmer({
        closable: false
    });
});

/**
 * Check whether the graph has loaded and apply/remove the dimmer
 * @param {boolean} loading The flag to determine whether to show or hide the dimmer
 */
function isLoading(loading) {
    if (loading) {
        $('#page-dimmer').dimmer('show')
    } else {
        $('#page-dimmer').dimmer('hide')
    };
}
