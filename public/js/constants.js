export const Constants = {
    REGULONDB_API_ROUTE: '/reglulondb',
    REGPRECICE_API_ROUTE: '/regprecise',
    VIS_EDGE_COLOURS: {
        GO_TERM_COLOURS: {
            'catalytic activity': '#8dd3c7',
            'binding': '#bebada',
            'transporter activity': '#fb8072',
            'molecular function regulator': '#80b1d3',
            'transcription regulator activity': '#fdb462',
            'molecular carrier activity': '#b3de69',
            'molecular transducer activity': '#fccde5',
            'cargo receptor activity': '#d9d9d9',
            'translation regulator activity': '#bc80bd',
            'protein tag': '#ccebc5',
            'antioxidant activity': '#a6cee3',
            'structural molecule activity': '#1f78b4',
            'molecular_function': '#b2df8a',
            'hijacked molecular function': '#33a02c',
            'go term unknown': '#666666'
        },
        DEFAULT: '#98C2FC'
    },
    VIS_NODE_COLOURS: {
        HIGHLIGHTED: '#faff00',
        SELECTED: '#01F601',
        DEFAULT: '#98C2FC'
    },
    EFFECT_EDGE_COLOURS: {

    },
    EVIDENCE_CODE_EDGE_OPACITIES: {

    }

};
