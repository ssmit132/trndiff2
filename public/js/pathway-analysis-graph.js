export const goTermColors = {
    'catalytic activity': '#8dd3c7',
    'binding': '#bebada',
    'transporter activity': '#fb8072',
    'molecular function regulator': '#80b1d3',
    'transcription regulator activity': '#fdb462',
    'molecular carrier activity': '#b3de69',
    'molecular transducer activity': '#fccde5',
    'cargo receptor activity': '#d9d9d9',
    'translation regulator activity': '#bc80bd',
    'protein tag': '#ccebc5',
    'antioxidant activity': '#a6cee3',
    'structural molecule activity': '#1f78b4',
    'molecular_function': '#b2df8a',
    'hijacked molecular function': '#33a02c',
    'go term unknown': '#666666'
};

const UNKNOWN_GO_TERM_COLOUR = '#666666';
const opacity = '0.25'; // Opacity for dimmed graph
let graphDimmed = false;
let tooltipCleared = true;

/**
 * The functionality to create the graph and the vis network
 * @param {string} pathway The pathway of the selected genome 
 * @param {element} graphEl The element of the graph
 * @param {element} tooltip The element of the graph tooltip
 * @param {element} loadingEl The loading spinner element
 * @param {string} apiUrl Flag determining whether regprecise or regulondb is used
 * @param {*} genomeId The genome id of the graph
 */
export function createGraph(pathway, graphEl, tooltip, loadingEl, apiUrl, genomeId) {
    let data = {
        nodes: new vis.DataSet(),
        edges: new vis.DataSet()
    };
    findGraph(pathway, data);
    let options = {
        interaction: {
            hover: true,
            dragNodes: true
        },
        groups: {
            Genes: {
                shape: 'box',
                color: 'rgb(255,165,0)',
                font: { color: 'rgb(0,0,0)' }
            },
            TFs: {
                color: 'rgb(152,194,252)',
                font: { color: 'rgb(0,0,0)' }
            },
            dimmedTFs: {
                color: 'rgb(152,194,252,' + opacity + ')',
                font: { color: 'rgb(0,0,0,' + opacity + ')' }
            },
            dimmedGenes: {
                shape: 'box',
                color: 'rgb(255,165,0,' + opacity + ')',
                font: { color: 'rgb(0,0,0,' + opacity + ')' }
            },
            highlightedTFs: {
                font: { color: 'rgb(0,0,0)' },
                color: 'rgb(7,249,55)'

            },
            highlightedGenes: {
                shape: 'box',
                font: { color: 'rgb(0,0,0)' },
                color: { background: '#07f937' }
            }
        },
        edges: {
            color: {
                inherit: false,
                color: '#98C2FC',
                highlight: '#98C2FC',
                hover: '#98C2FC'
            }
        }
    };
    let graph = new vis.Network(graphEl[0], data, options);

    let buttonGroup = $('<a>').addClass('ui bottom right attached label group-icon-t');

    // Graph freeze button
    buttonGroup.append($('<i>').addClass('stop circle outline icon').attr('title', 'stop physics').click(function(e) {
        if ($(this).hasClass('stop')) {
            $(this).removeClass('stop circle outline');
            $(this).addClass('play');
            $(this).attr('title', 'start physics');

            let options = {
                physics: {
                    enabled: false
                }
            };
            graph.setOptions(options);

        } else {
            $(this).removeClass('play');
            $(this).addClass('stop circle outline');
            $(this).attr('title', 'stop physics');

            let options = {
                physics: {
                    enabled: true
                }
            };
            graph.setOptions(options);
        }

        graph.stopSimulation();
        e.stopPropagation();
    }));

    // Graph legend button
    buttonGroup.append($('<i>').addClass('info circle icon graph-legend').attr('title', 'display legend').click(function(e) {
        $('#showLegendModal').modal('show');
        e.stopPropagation();
    }));

    // Graph save button
    var link = document.createElement('a');
    // let specificClass =
    buttonGroup.append($('<i>').addClass('save icon').attr('title', 'export graph').click(function(e) {
        let edgeUpdates = [];
        e.stopPropagation();
        data.edges.forEach(e => {
            edgeUpdates.push({ id: e.id, width: 4 });
        });
        data.edges.update(edgeUpdates);

        graph.once('stabilized', function() {
            // Download the image
            link.download = 'graph_snapshot';
            let can = document.getElementsByTagName('canvas');
            let canvas = can[0];

            let canvasElement = genomeId && $(`#${genomeId}.genome`);
            if (!canvasElement || !canvasElement[0]) return;
            canvas = canvasElement[0].getElementsByTagName('canvas')[0];

            let uri = canvas.toDataURL('image/png');
            link.href = uri;
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);

            edgeUpdates = [];
            data.edges.forEach(e => {
                edgeUpdates.push({ id: e.id, width: 1 });
            });
            data.edges.update(edgeUpdates);
        });
    }));

    // Graph expand button
    buttonGroup.append($('<i>').addClass('expand icon').attr('title', 'fit graph').click(function(e) {
        graph.fit();
        e.stopPropagation();
    }));

    graphEl.append(buttonGroup);

    graph.on('selectNode', function(e) {
        let node = data.nodes.get().find(n => n.id === e.nodes[0]);
        if (node.group.includes('TFs')) {
            let regulonHovered = pathway['selected-regulons'].find(r => r.regulatorName.toLowerCase() === node.id);
            let exploreTFButton;
            if (!regulonHovered) {
                for (let regulon of pathway['selected-regulons']) {
                    regulonHovered = [...regulon.regulatingTFs, ...regulon.targetTFs].find(tf => tf.name.toLowerCase() === node.id);
                    if (regulonHovered) {
                        break;
                    }
                }
                if (regulonHovered) {
                    exploreTFButton = document.createElement('button');
                    exploreTFButton.innerHTML = 'Explore TF Pathway';
                    exploreTFButton.style = 'background-color: #98C2FC; border: 2px solid grey;border-radius: 4px; color: black;padding: 5px 10px; text-align: center;  text-decoration: none; display: inline-block;font-size: 16px;box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2), 0 6px 20px 0 rgba(0,0,0,0.19)';
                    exploreTFButton.onclick = function() { exploreTF(regulonHovered.regulonId || regulonHovered.name || regulonHovered.regulatorName, data, pathway, apiUrl); };
                }
            }
            if (regulonHovered) {
                let reloadButton = document.createElement('button');
                reloadButton.innerHTML = 'Reload Pathway';
                reloadButton.style = 'background-color: #98C2FC; border: 2px solid grey;border-radius: 4px; color: black;padding: 5px 10px; text-align: center;  text-decoration: none; display: inline-block;font-size: 16px;box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2), 0 6px 20px 0 rgba(0,0,0,0.19)';
                reloadButton.onclick = function() { regulogReload(regulonHovered.regulonId || regulonHovered.name || regulonHovered.regulatorName); };
                tooltip.empty();
                let tableEl = $('<table>');
                if (regulonHovered.regulatorName || regulonHovered.name) {
                    tableEl.append($('<tr>')
                        .append($('<td>').text('Regulator Name:'))
                        .append($('<td>').text(regulonHovered.regulatorName || regulonHovered.name)));
                }
                if (regulonHovered.regulatorFamily) {
                    tableEl.append($('<tr>')
                        .append($('<td>').text('Regulator Family:'))
                        .append($('<td>').text(regulonHovered.regulatorFamily)));
                }
                if (regulonHovered.regulationType) {
                    tableEl.append($('<tr>')
                        .append($('<td>').text('Regulation Type:'))
                        .append($('<td>').text(regulonHovered.regulationType || 'n/a')));
                }
                if (regulonHovered.effector) {
                    tableEl.append($('<tr>')
                        .append($('<td>').text('Effector:'))
                        .append($('<td>').text(regulonHovered.effector || 'n/a')));
                }
                if (regulonHovered.pathway) {
                    tableEl.append($('<tr>')
                        .append($('<td>').text('Pathway:'))
                        .append($('<td>').text(regulonHovered.pathway || 'n/a')));
                }

                if (apiUrl && apiUrl.toLowerCase().includes('regprecise')) {
                    let regulogButton = document.createElement('button');
                    regulogButton.innerHTML = 'View Regulog Graph';
                    regulogButton.style = 'background-color: #98C2FC;border: 2px solid grey;border-radius: 4px; color: black;padding: 5px 10px; text-align: center;  text-decoration: none; display: inline-block;font-size: 16px;box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2), 0 6px 20px 0 rgba(0,0,0,0.19)';
                    regulogButton.onclick = function() { regulogNavigation(regulonHovered.regulonId); };

                    tableEl.append($('<tr>')
                        .append($('<td colspan=2>').append($(regulogButton))));
                }

                if (exploreTFButton) {
                    tableEl.append($('<tr>')
                        .append($('<td colspan=2>').append($(exploreTFButton))));
                }

                tableEl.append($('<tr>')
                    .append($('<td colspan=2>').append($(reloadButton))));
                tooltip.append(tableEl);
                tooltip.css('visibility', 'visible');
                tooltip.css('top', (event.pageY - 20) + 'px').css('left', (event.pageX + 20) + 'px');
                tooltipCleared = false;
            }
        } else {
            let geneHovered;
            for (let regulon of pathway['selected-regulons']) {
                geneHovered = regulon.targetGenes.find(tg => tg.name.toLowerCase() === node.id);
                if (geneHovered) break;
            }
            if (geneHovered) {
                tooltip.empty();
                let tableEl = $('<table>');
                if (geneHovered.name) {
                    tableEl.append($('<tr>')
                        .append($('<td>').text('Gene Name:'))
                        .append($('<td>').text(geneHovered.name || 'n/a')));
                }
                if (geneHovered.function) {
                    tableEl.append($('<tr>')
                        .append($('<td>').text('Function:'))
                        .append($('<td>').text(geneHovered.function || 'n/a')));
                }
                if (geneHovered.locusTag) {
                    tableEl.append($('<tr>')
                        .append($('<td>').text('Locus Tag:'))
                        .append($('<td>').text(geneHovered.locusTag || 'n/a')));
                }
                if (geneHovered.vimssId) {
                    tableEl.append($('<tr>')
                        .append($('<td>').text('Vimss ID:'))
                        .append($('<td>').text(geneHovered.vimssId || 'n/a')));
                }
                if (geneHovered.strength) {
                    tableEl.append($('<tr>')
                        .append($('<td>').text('Strength:'))
                        .append($('<td>').text(geneHovered.strength || 'n/a')));
                }
                if (geneHovered.effect) {
                    tableEl.append($('<tr>')
                        .append($('<td>').text('Effect:'))
                        .append($('<td>').text(geneHovered.effect || 'n/a')));
                }
                if (geneHovered.evidence) {
                    tableEl.append($('<tr>')
                        .append($('<td>').text('Evidence Codes:'))
                        .append($('<td>').text(geneHovered.evidence || 'n/a')));
                }
                tooltip.append(tableEl);
                tooltip.css('visibility', 'visible');
                tooltip.css('top', (event.pageY - 20) + 'px').css('left', (event.pageX + 20) + 'px');
                tooltipCleared = false;
            }
        }
    });

    graph.on('click', function(e) {
        if (e && e.nodes && e.nodes.length > 0) return;
        let edge = e && e.edges && e.edges[0] && data.edges.get().find(edge => edge.id === e.edges[0]);
        if (!edge) return;
        tooltip.empty();
        let tableEl = $('<table>');
        if (edge.strength) {
            tableEl.append($('<tr>')
                .append($('<td>').text('Strength:'))
                .append($('<td>').text(edge.strength || 'n/a')));
        }
        if (edge.effect) {
            tableEl.append($('<tr>')
                .append($('<td>').text('Effect:'))
                .append($('<td>').text(edge.effect || 'n/a')));
        }
        if (edge.evidenceCodes) {
            tableEl.append($('<tr>')
                .append($('<td>').text('Evidence Codes:'))
                .append($('<td>').text(edge.evidenceCodes || 'n/a')));
        }
        if (edge.goTerm) {
            tableEl.append($('<tr>')
                .append($('<td>').text('Go Term:'))
                .append($('<td>').text(edge.goTerm || 'n/a')));
        }
        tooltip.append(tableEl);
        tooltip.css('visibility', 'visible');
        tooltip.css('top', (event.pageY - 20) + 'px').css('left', (event.pageX + 20) + 'px');
        tooltipCleared = false;
    });

    graph.on('deselectNode', function(_e) {
        tooltip.css('visibility', 'hidden');
    });

    graph.on('deselectEdge', function(_e) {
        tooltip.css('visibility', 'hidden');
    });

    graph.on('afterDrawing', () => {
        $(loadingEl).dimmer('hide');
    });

    // Dimming Functionality
    graph.on('click', function(clickInfo) {
        // If the user clicked a node
        if (clickInfo.nodes.length !== 0) {
            // Convert the node to the network format
            let node;
            data.nodes.forEach(n => {
                if (n.id.toLowerCase() === clickInfo.nodes[0].toLowerCase()) {
                    node = n;
                }
            });

            // If the node is already selected
            if (getUndimmedNodes(data).length > 0 && !singleNodeDimmed(node) && graphDimmed) {
                // Dim the node
                let singleNode = [node];
                toggleNodeDim(singleNode, data, graph);

                // If no undimmed nodes left, unndim whole graph
                if (getUndimmedNodes(data).length === 0) {
                    undimEverything(data);
                    graphDimmed = false;
                }
            } else {
                // If there are no dimmed nodes, dim whole graph
                if (!graphDimmed) {
                    dimEverything(data);
                    graphDimmed = true;

                    // Update node with new properties
                    data.nodes.forEach(n => {
                        if (n.id.toLowerCase() === clickInfo.nodes[0].toLowerCase()) {
                            node = n;
                        }
                    });
                }
                // Undim single node

                let singleNode = [node];
                toggleNodeDim(singleNode, data, graph);
            }
        } else if (clickInfo.edges.length !== 0) {
            let edge;
            data.edges.forEach(e => {
                if (e.id === clickInfo.edges[0]) {
                    edge = e;
                }
            });

            // If the edge is already selected
            if (getUndimmedNodes(data).length > 0 && !edge.dimmed && graphDimmed) {
                toggleEdgeDim(edge, data, graph);

                // If no undimmed nodes left, unndim whole graph
                if (getUndimmedNodes(data).length === 0) {
                    undimEverything(data);
                    graphDimmed = false;
                }
            } else {
                // If there are no dimmed nodes, dim whole graph
                if (!graphDimmed) {
                    dimEverything(data);
                    graphDimmed = true;

                    // Update the edge with new properties
                    data.edges.forEach(e => {
                        if (e.id === clickInfo.edges[0]) {
                            edge = e;
                        }
                    });
                }

                // Undim edge and its connected nodes
                toggleEdgeDim(edge, data, graph);
            }
        } else {
            if (tooltipCleared) {
                undimEverything(data);
                graphDimmed = false;
            }
            tooltipCleared = true;
        }
    });

    return [data, graph];
}

/**
 * Functionality to check if a single node is dimmed
 * @param {visData} node The node to be checked for dimming
 */
function singleNodeDimmed(node) {
    if (node.group === 'dimmedGenes' || node.group === 'dimmedTFs') {
        return true;
    } else {
        return false;
    }
}

/**
 * Functionality to get all dimmed nodes in the graph
 * @param {VisDataSet} data The data containing the noed information
 */
function getUndimmedNodes(data) {
    let nodes = [];

    data.nodes.forEach(n => {
        if (n.group === 'Genes' || n.group === 'TFs') {
            nodes.push(n);
        }
    });

    return nodes;
}

/**
 * Functionality to dim/undiim an edge and its connected nodes
 * @param {visData} edge The edge selected by the user thats dim will be toggledz
 * @param {visDataSet} data The vis data of the network
 * @param {visDataSet} pathwayNetwork The vis graph data
 */
function toggleEdgeDim(edge, data, pathwayNetwork) {
    let nodeUpdates = [];
    let edgeUpdates = [];
    let connectedNodes = [];

    // Get all of the edge's connected nodes and store them in an array
    let nodes = pathwayNetwork.getConnectedNodes(edge.id);
    nodes.forEach(n => {
        // Find node in the data and add to connected nodes array for processing
        data.nodes.forEach(dataNode => {
            if (dataNode.id === n) {
                connectedNodes.push(dataNode);
            }
        });
    });

    // If the edge is not selected
    if (edge.dimmed) {
        let newEdge = { color: edge.color.color, highlight: edge.color.highlight, hover: edge.color.hover, opacity: '1' };
        edgeUpdates.push({ id: edge.id, color: newEdge, dimmed: false });

        connectedNodes.forEach(node => {
            // If the node is a gene
            if (node.group === 'Genes' || node.group === 'dimmedGenes') {
                nodeUpdates.push({ id: node.id, group: 'Genes' });
            } else {
                nodeUpdates.push({ id: node.id, group: 'TFs' });
            }
        });
    } else {
        let newEdge = { color: edge.color.color, highlight: edge.color.highlight, hover: edge.color.hover, opacity: '0.25' };
        edgeUpdates.push({ id: edge.id, color: newEdge, dimmed: true });

        connectedNodes.forEach(node => {
            if (!hasOtherUndimmedEdges(node, edge, data, pathwayNetwork)) {
                // If the node is a gene
                if (node.group === 'Genes' || node.group === 'dimmedGenes') {
                    nodeUpdates.push({ id: node.id, group: 'dimmedGenes' });
                } else {
                    nodeUpdates.push({ id: node.id, group: 'dimmedTFs' });
                }
            }
        });
    }

    data.edges.update(edgeUpdates);
    data.nodes.update(nodeUpdates);
}

 /**
  * Functionality to check whether an edge's nodes are connected to any other undimmed edges
  * @param {visData} node The selected node
  * @param {visData} selectedEdge The edge if selected
  * @param {visDataSet} data The vis data of the network
  * @param {visDataSet} pathwayNetwork The vis graph data 
  */
function hasOtherUndimmedEdges(node, selectedEdge, data, pathwayNetwork) {
    let hasUndimmedEdges = false;

    // Get all of the edge's connected nodes and store them in an array
    let edges = pathwayNetwork.getConnectedEdges(node.id);
    edges.forEach(edge => {
        // Find node in the data and add to connected nodes array for processing
        data.edges.forEach(e => {
            if (edge === e.id && edge !== selectedEdge.id) {
                if (!e.dimmed) {
                    hasUndimmedEdges = true;
                }
            }
        });
    });
    return hasUndimmedEdges;
}

/**
 * Functioanlity to dim/undim selected nodes
 * @param {visData} nodes The node selected for dimming
 * @param {visDataSet} data The vis data of the network
 * @param {visDataSet} pathwayNetwork The vis graph data
 */
function toggleNodeDim(nodes, data, pathwayNetwork) {
    let nodeUpdates = [];
    let edgeUpdates = [];
    let undimming = false;
    nodes.forEach(node => {
        // If the node is a gene
        if (node.group === 'Genes' || node.group === 'dimmedGenes') {
            // If the node is not dimmed
            if (node.group === 'Genes') {
                nodeUpdates.push({ id: node.id, group: 'dimmedGenes' });

                // Dim all of the nodes connected edges
                let edges = pathwayNetwork.getConnectedEdges(node.id);
                edges.forEach(edge => {
                    // Find edge in the data
                    let dataEdgeFound;
                    data.edges.forEach(dataEdge => {
                        if (dataEdge.id === edge) {
                            dataEdgeFound = dataEdge;
                        }
                    });
                    let newEdge = { color: dataEdgeFound.color.color, highlight: dataEdgeFound.color.highlight, hover: dataEdgeFound.color.hover, opacity: opacity };
                    edgeUpdates.push({ id: edge, color: newEdge, dimmed: true });
                });
            } else {
                nodeUpdates.push({ id: node.id, group: 'Genes' });
                undimming = true;
            }
        } else {
            // If the node is not dimmed
            if (node.group === 'TFs') {
                nodeUpdates.push({ id: node.id, group: 'dimmedTFs' });

                // Dim all of the nodes connected edges
                let edges = pathwayNetwork.getConnectedEdges(node.id);
                edges.forEach(edge => {
                    // Find edge in the data
                    let dataEdgeFound;
                    data.edges.forEach(dataEdge => {
                        if (dataEdge.id === edge) {
                            dataEdgeFound = dataEdge;
                        }
                    });
                    let newEdge = { color: dataEdgeFound.color.color, highlight: dataEdgeFound.color.highlight, hover: dataEdgeFound.color.hover, opacity: opacity };
                    edgeUpdates.push({ id: edge, color: newEdge, dimmed: true });
                });
            } else {
                nodeUpdates.push({ id: node.id, group: 'TFs' });
                undimming = true;
            }
        }
    });

    data.nodes.update(nodeUpdates);
    data.edges.update(edgeUpdates);

    if (undimming) {
        undimConnectedEdges(data, pathwayNetwork);
    }
}

/**
 * Functionality to undim all of the directly connected edges of the undimmed nodes
 * @param {visDataSet} data The vis data of the network
 * @param {visDataSet} pathwayNetwork The vis graph data
 */
function undimConnectedEdges(data, pathwayNetwork) {
    let edgeUpdates = [];
    let edgeList = [];

    let undimmedNodes = getUndimmedNodes(data);

    // Iterate through all undimmed nodes
    undimmedNodes.forEach(node => {
        // Get the edges connected to the undimmed node
        let edges = pathwayNetwork.getConnectedEdges(node.id);
        edges.forEach(edge => {
            // If the edge isn't already on the list of edges, add it
            if (edgeList.indexOf(edge) === -1) {
                edgeList.push(edge);
            } else {
                // Find edge in the data
                let dataEdgeFound;
                data.edges.forEach(dataEdge => {
                    if (dataEdge.id === edge) {
                        dataEdgeFound = dataEdge;
                    }
                });

                let newEdge = { color: dataEdgeFound.color.color, highlight: dataEdgeFound.color.highlight, hover: dataEdgeFound.color.hover, opacity: '1' };
                edgeUpdates.push({ id: dataEdgeFound.id, color: newEdge, dimmed: false });
            }
        });
    });
    data.edges.update(edgeUpdates);
}

/**
 * Functionality to undim all nodes and edges in the graph
 * @param {visDataSet} data The vis data of the network
 */
function undimEverything(data) {
    let nodeUpdates = [];
    let edgeUpdates = [];

    // Undim all nodes
    data.nodes.forEach(node => {
        if (node.group === 'dimmedGenes' || node.group === 'highlightedGenes') {
            nodeUpdates.push({ id: node.id, group: 'Genes' });
        } else if (node.group === 'dimmedTFs' || node.group === 'highlightedTFs') {
            nodeUpdates.push({ id: node.id, group: 'TFs' });
        }
    });

    // Undim all edges
    data.edges.forEach(edge => {
        let newEdge = { color: edge.color.color, highlight: edge.color.highlight, hover: edge.color.hover, opacity: '1' };
        edgeUpdates.push({ id: edge.id, color: newEdge, dimmed: false });
    });

    data.nodes.update(nodeUpdates);
    data.edges.update(edgeUpdates);
}

/**
 * Functionality to dim all nodes and edges in the graph
 * @param {visDataSet} data The vis data of the network
 */
function dimEverything(data) {
    let nodeUpdates = [];
    let edgeUpdates = [];

    // Dim all nodes
    data.nodes.forEach(node => {
        if (node.group === 'Genes' || node.group === 'highlightedGenes') {
            nodeUpdates.push({ id: node.id.toLowerCase(), group: 'dimmedGenes', dimmed: true });
        } else {
            nodeUpdates.push({ id: node.id.toLowerCase(), group: 'dimmedTFs', dimmed: true });
        }
    });

    // Dim all edges
    data.edges.forEach(edge => {
        let newEdge = { color: edge.color.color, highlight: edge.color.highlight, hover: edge.color.hover, opacity: opacity };
        edgeUpdates.push({ id: edge.id, color: newEdge, dimmed: true });
    });

    data.nodes.update(nodeUpdates);
    data.edges.update(edgeUpdates);
}

function regulogNavigation(regulonId) {
    location.href = `reguloggraph?regulonId=${regulonId}`;
}


function regulogReload(regulonId) {
    location.href = `pathway?regulons=${regulonId}`;
}

/**
 * Functionality to toggle directional arrows on graph edges
 * @param {boolean} displayArrows The flag that determines whether the arrows are enabled or disabled
 * @param {visDataSet} data The vis data of the network
 */
export function toggleEdgeArrows(displayArrows, data) {
    let updates = [];
    const arrow = { to: { enabled: displayArrows } };
    let edges = data.edges.get();

    for (let edge of edges) {
        updates.push({ id: edge.id, from: edge.from, to: edge.to, arrows: arrow });
    }
    data.edges.update(updates);
}

/**
 *
 * @param {The regulons selected by the user for analysis} tfPathway
 * @param {visDataSet} data The vis data of the network
 */
function findGraph(tfPathway, data) {
    tfPathway['selected-regulons'].forEach(regulon => {
        if (data && data.nodes && !data.nodes.get().find(n => n.id === regulon.regulatorName.toLowerCase())) {
            data.nodes.add({
                id: regulon.regulatorName.toLowerCase(),
                label: regulon.regulatorName,
                group: 'TFs'
            });
        }

        regulon.targetGenes.forEach(g => {
            let colour = goTermColors[g.goTerm] ? goTermColors[g.goTerm] : UNKNOWN_GO_TERM_COLOUR;
            const edge = {
                from: regulon.regulatorName.toLowerCase(),
                to: g.name.toLowerCase(),
                width: 2,
                color: { color: colour, highlight: colour, hover: colour },
                dimmed: false,
                effect: g.effect,
                evidenceCodes: g.evidence,
                strength: g.strength,
                goTerm: g.goTerm
            };
            if (data && data.edges && !data.edges.get().find(e => e.to === edge.to && e.from === edge.from)) {
                data.edges.add(edge);
            }

            if (data && data.nodes && !data.nodes.get().find(n => n.id === g.name.toLowerCase())) {
                data.nodes.add({
                    id: g.name.toLowerCase(),
                    label: g.name,
                    group: 'Genes'
                });
            };
        });

        regulon.targetTFs.forEach(tf => {
            let colour = goTermColors[tf.goTerm] ? goTermColors[tf.goTerm] : UNKNOWN_GO_TERM_COLOUR;
            const edge = {
                from: regulon.regulatorName.toLowerCase(),
                to: tf.name.toLowerCase(),
                width: 2,
                color: { color: colour, highlight: colour, hover: colour },
                dimmed: false,
                effect: tf.effect,
                evidenceCodes: tf.evidence,
                strength: tf.strength,
                goTerm: tf.goTerm
            };
            if (data && data.edges && !data.edges.get().find(e => e.to === edge.to && e.from === edge.from)) {
                data.edges.add(edge);
            }

            if (data && data.nodes && !data.nodes.get().find(n => n.id === tf.name.toLowerCase())) {
                data.nodes.add({
                    id: tf.name.toLowerCase(),
                    label: tf.name,
                    group: 'TFs'
                });
            }
        });

        regulon.regulatingTFs.forEach(tf => {
            let colour = goTermColors[tf.goTerm] ? goTermColors[tf.goTerm] : UNKNOWN_GO_TERM_COLOUR;
            const edge = {
                width: 2,
                from: tf.name.toLowerCase(),
                to: regulon.regulatorName.toLowerCase(),
                color: { color: colour, highlight: colour, hover: colour },
                dimmed: false,
                effect: tf.effect,
                evidenceCodes: tf.evidence,
                strength: tf.strength,
                goTerm: tf.goTerm
            };
            if (data && data.edges && !data.edges.get().find(e => e.to === edge.to && e.from === edge.from)) {
                data.edges.add(edge);
            }

            if (data && data.nodes && !data.nodes.get().find(n => n.id === tf.name.toLowerCase())) {
                data.nodes.add({
                    id: tf.name.toLowerCase(),
                    label: tf.name,
                    group: 'TFs'
                });
            }
        });
    });
}

/**
 *
 * @param {The ID of the regulon to be added to the vis network graph} regulonId
 * @param {visDataSet} data The vis data of the network
 * @param {visDataSet} pathwayNetwork The vis graph data
 */
function exploreTF(regulonId, data, pathway, apiUrl) {
    $.ajax({
        type: 'GET',
        url: `${apiUrl}pathway?regulons=${regulonId}`,
        contentType: 'application/json',
        success: function(tfPathway) {
            findGraph(tfPathway, data);
            pathway['selected-regulons'].push(tfPathway['selected-regulons'][0]);
        }
    });
}

/**
 * Create the comparison graph for extra genomes
 * @param {visDataSet} network The vis network
 */
export function createComparisonGraph(network) {
    let data = {
        nodes: new vis.DataSet(),
        edges: new vis.DataSet()
    };
    let graph = $('#comparison-graph');
    findGraph(network, data);
    let options = {
        groups: {
            Genes: {
                shape: 'box',
                color: 'rgb(255,165,0)',
                font: { color: 'rgb(0,0,0)' }
            },
            TFs: {
                shape: 'circle',
                color: 'rgb(152,194,252)',
                font: { color: 'rgb(0,0,0)' }
            }
        }
    };
    let _pathwayNetwork = new vis.Network(graph[0], data, options);
}

/**
 *
 * Filters the displayed Regulons via all filter options
 * @param {string[]} selectedEvidenceCodes  The list of Evidence Codes being filtered.
 * @param {string[]} selectedRegulatoryEffects  The list of Regulatory Effects being filtered.
 * @param {boolean} selfRegulatingHidden The value to determine whether to filter self-regulating tfs
 * @param {boolean} unregulatedHidden The value to determine whether to filter unregulated tfs
 * @param {VisDataSet} visGraphData The vis data set to be updated.
 * @param {boolean} onlySelfRegulating The value to determine whether to only filter by self-regulating tfs
 */
export function applyFilters(selectedEvidenceCodes, selectedRegulatoryEffects, visGraph, targetInput, regulatorInput, selfRegulatedState, unregulatedState, selectedGoTerm, displayEdges) {
    let edgeUpdates = [];
    let nodeUpdates = [];
    const nodes = visGraph.body.data.nodes.get();
    nodes.forEach(node => {
        const connectedEdgeIds = visGraph.getConnectedEdges(node.id);
        const connectedEdges = visGraph.body.data.edges.get(connectedEdgeIds);
        let displayNode;
        if (node.group && node.group.includes('Genes')) {
            displayNode = checkUnregulated(connectedEdges, unregulatedState) && checkSelfRegualted(connectedEdges, selfRegulatedState) && ((!targetInput && targetInput === '') || (node.id.toLowerCase().includes(targetInput.toLowerCase())));
        } else if (node.group && node.group.includes('TFs')) {
            displayNode = checkUnregulated(connectedEdges, unregulatedState) && checkSelfRegualted(connectedEdges, selfRegulatedState) && ((!regulatorInput && regulatorInput === '') || (node.id.toLowerCase().includes(regulatorInput.toLowerCase())));
        }
        // Only check edges if the node is visible, otherwise theres no point
        if (displayNode) {
            connectedEdges.forEach(edge => {
                let displayEdge = true;
                if (!displayEdges && (!selectedEvidenceCodes || selectedEvidenceCodes.length === 0) && (!selectedRegulatoryEffects || selectedRegulatoryEffects.length === 0) && (!selectedGoTerm)) {
                    displayEdge = true;
                } else if (
                    (selectedEvidenceCodes && selectedEvidenceCodes.length > 0 && evidenceFilter(selectedEvidenceCodes, edge.evidenceCodes)) ||
                    (selectedRegulatoryEffects && selectedRegulatoryEffects.length > 0 && edge.effect && !(selectedRegulatoryEffects.includes(edge.effect))) ||
                    (selectedGoTerm && (!edge.goTerm || (edge.goTerm && selectedGoTerm !== edge.goTerm.replace(/\s/g, ''))))) {
                    displayEdge = false;
                }

                edgeUpdates.push({
                    id: edge.id,
                    hidden: !displayEdge
                });
            });
        }

        // Check if all edges were hidden
        if (displayNode) {
            nodeUpdates.push({ id: node.id, hidden: false, physics: true });
        } else { // If the node passed the filter but is hidden, unhide it
            nodeUpdates.push({ id: node.id, hidden: true, physics: false });
        }
    });

    // Update the graph
    visGraph.body.data.nodes.update(nodeUpdates);
    visGraph.body.data.edges.update(edgeUpdates);
}

/**
 * Check if a node is self regulated for resuming state
 * @param {visData} connectedEdges The connected edges of a node
 * @param {boolean} displayState The display state of the graph
 */
function checkSelfRegualted(connectedEdges, displayState) {
    let displayNode = true;
    switch (displayState) {
    case 'hide':
        displayNode = !(connectedEdges.find(e => e.to === e.from));
        break;
    case 'only':
        displayNode = !!(connectedEdges.find(e => e.to === e.from));
        break;
    }
    return displayNode;
}

/**
 * Check if a node is unregulated for resuming state
 * @param {visData} connectedEdges The connected edges of a node
 * @param {boolean} displayState The display state of the graph
 */
function checkUnregulated(connectedEdges, displayState) {
    let displayNode = true;
    switch (displayState) {
    case 'hide':
        displayNode = connectedEdges.length > 0;
        break;
    case 'only':
        displayNode = connectedEdges.length === 0;
        break;
    }
    return displayNode;
}

/**
 * Decide whether or not to filter evidence code
 * @param {string[]} selectedEvidenceCodes The selected evidence codes
 * @param {string[]} evidenceCodes The evidence codes
 */
function evidenceFilter(selectedEvidenceCodes, evidenceCodes) {
    let hide = true;
    for (let code of evidenceCodes) {
        if (selectedEvidenceCodes.find(c => c.toLowerCase() === code.toLowerCase())) {
            hide = false;
            break;
        }
    }
    return hide;
}
