export function createUrl(additionalOptions = {}) {
    // Get the specific filter options
    const regulatorInputVal = $('#header #search-node #regulator-search').val();
    const targetInputVal = $('#header #search-node #target-search').val();
    const selfRegulatedState = $('#group-self-regulated .blue').attr('name');
    const unregulatedState = $('#group-hide-unregulated .blue').attr('name');
    const directionalArrows = $('#toggle-arrows.checkbox').checkbox('is checked');
    const removePathways = $('#header #remove-pathways.checkbox')[0] && $('#header #remove-pathways.checkbox').checkbox('is checked');
    const selectedEvidenceCodes = $('#header #evidence-values').val();
    const selectedEffects = $('#header #regulatory-effect').val();
    const selectedGenomes = $('#header #genomes-selected').val();
    const regulonDB = $('#compare-regulondb')[0] && $('#compare-regulondb').checkbox('is checked');

    const options = {
        ...additionalOptions,
        regulatorinput: regulatorInputVal,
        targetinput: targetInputVal,
        selfregulatedstate: selfRegulatedState !== 'show' ? selfRegulatedState : undefined,
        unregulatedstate: unregulatedState !== 'show' ? unregulatedState : undefined,
        alternategenomes: selectedGenomes && selectedGenomes.length > 0 ? selectedGenomes.join('+') : undefined,
        directionalarrows: directionalArrows || undefined,
        removepathways: removePathways || undefined,
        effects: selectedEffects && selectedEffects.length > 0 ? selectedEffects.join('+') : undefined,
        evidencecodes: selectedEvidenceCodes && selectedEvidenceCodes.length > 0 ? selectedEvidenceCodes.join('+') : undefined,
        regulondb: regulonDB || undefined
    };

    let paramsList = [];
    Object.keys(options).forEach(key => {
        if (options[key] && options[key] !== undefined && options[key] !== '') {
            paramsList.push(`${key}=${options[key]}`);
        }
    });

    const newParams = paramsList.join('&');
    let url = `${window.location.pathname}`;
    if (newParams !== '') url = `${url}?${newParams}`;
    history.replaceState(options, 'TRNDiff', url);

    $('#export-view-input').val(`${window.location.origin}${url}`);
}

export function exportView() {
    console.log('exporting');
    let input = document.getElementById('export-view-input');
    input.select();
    console.log(input);
    document.execCommand('copy');
}
