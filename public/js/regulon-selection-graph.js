import { Constants } from './constants.js';

const selectedEmptyLabel = 'No Regulons Selected. Select any number of Regulons to view the Pathway Analysis, or select a single Regulon to view the related Regulog Graph. Click the links above to view the corresponding page';
/**
 * Creates the graph for the regulon network and sets basic event handlers.
 * @param {Regulon network structure} network   The regulon network to be mapped. Must have target regulators and target genes.
 * @param {VisNetwork} graphEl   The jquery element to be filled with the graph.
 * @param {jQuery} loadingEl The graph's loading spinner jquery element.
 * @param {jQuery} tooltipEl The tooltip jquery element for display node details.
 * @param {string} apiUrl The url of the api route module used for the graph.
 * @param {jQuery} clearButtonEl The jquery element used to clear all selected nodes.
 * @returns {[VisNetwork, VisDataSet]} Returns the vis network and data set for the regulon network provided
 */
export function createGraph(network, graphEl, loadingEl, tooltipEl, apiUrl, clearButtonEl) {
    let data = {
        nodes: new vis.DataSet(),
        edges: new vis.DataSet()
    };

    let options = {
        interaction: {
            hover: true
        },
        edges: {
            color: {
                inherit: false,
                color: Constants.VIS_NODE_COLOURS.DEFAULT,
                highlight: Constants.VIS_NODE_COLOURS.DEFAULT,
                hover: Constants.VIS_NODE_COLOURS.DEFAULT
            }
        },
        layout: { hierarchical: { enabled: false } },
        physics: {
            stabilization: { enabled: true, fit: true },
            timestep: 0.9
        }
    };

    findGraph(network, data);
    let graph = new vis.Network(graphEl[0], data, options);
    graph.on('afterDrawing', function() {
        loadingEl.dimmer('hide');
    });

    graph.on('click', function(e) {
        if (e.nodes.length === 0) return;
        selectNode(e.nodes[0], network, data, apiUrl, clearButtonEl);
    });

    // Group buttons at bottom right of graph
    graphEl.append($('<a>').addClass('ui bottom right attached label group-icon-t'));

    let buttonGroup = $('.group-icon-t');

    buttonGroup.append($('<i>').addClass('save icon'));
    var link = document.createElement('a');
    $('.save').click(function(e) {
        let edgeUpdates = [];
        data.edges.forEach(e => {
            edgeUpdates.push({ id: e.id, width: 4 });
        });
        data.edges.update(edgeUpdates);

        graph.once('stabilized', function() {

            // Download the image
            link.download = 'graph_snapshot';
            var can = document.getElementsByTagName('canvas');
            var uri = can[0].toDataURL('image/png');
            link.href = uri;
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);

            edgeUpdates = [];
            data.edges.forEach(e => {
                edgeUpdates.push({ id: e.id, width: 1 });
            });
            data.edges.update(edgeUpdates);
        });
    });

    buttonGroup.append($('<i>').addClass('expand icon'));
    $('.expand').click(function(e) {
        graph.fit();
        e.stopPropagation();
    });

    // graphEl.append($('<a>').addClass('ui bottom right attached label').append($('<i>').addClass('expand icon')).click(function(e) {
    //     graph.fit();
    //     e.stopPropagation();
    // }));

    // graphEl.append($('<a>').addClass('ui top right attached label').append($('<i>').addClass('info circle icon')).click(function(e) {
    //     $('#showLegendModal').modal('show');
    // }));

    graph.on('hoverNode', function(e) {
        let regulonHovered = network.find(r => r.regulatorName.toLowerCase().replace(/[/|&;$%@"<>()+,]/g, '') === e.node);
        if (regulonHovered == null) return;
        createNewTooltip(regulonHovered, tooltipEl);
        tooltipEl.css('visibility', 'visible');
        // Set position of the tooltip relative to the mouse location when first hovering on node
        tooltipEl.css('top', (event.pageY - 20) + 'px').css('left', (event.pageX + 20) + 'px');

        document.body.style.cursor = 'pointer';
    });

    graph.on('blurNode', function() {
        tooltipEl.css('visibility', 'hidden');
        document.body.style.cursor = 'default';
    });

    graph.on('hoverEdge', function(e) {
        let edge = data.edges.get().find(n => n.id === e.edge);
        tooltipEl.empty();
        let tableEl = $('<table>');
        if (edge.strength) {
            tableEl.append($('<tr>')
                .append($('<td>').text('Strength:'))
                .append($('<td>').text(edge.strength || 'n/a')));
        }
        if (edge.effect) {
            tableEl.append($('<tr>')
                .append($('<td>').text('Effect:'))
                .append($('<td>').text(edge.effect || 'n/a')));
        }
        if (edge.evidenceCodes) {
            tableEl.append($('<tr>')
                .append($('<td>').text('Evidence Codes:'))
                .append($('<td>').text(edge.evidenceCodes || 'n/a')));
        }
        if (edge.goTerm) {
            tableEl.append($('<tr>')
                .append($('<td>').text('Go Term:'))
                .append($('<td>').text(edge.goTerm || 'n/a')));
        }
        tooltipEl.append(tableEl);
        tooltipEl.css('visibility', 'visible');
        tooltipEl.css('top', (event.pageY - 20) + 'px').css('left', (event.pageX + 20) + 'px');
    });

    graph.on('blurEdge', function(_e) {
        tooltipEl.css('visibility', 'hidden');
    });

    return [graph, data];
}

/**
 * Clears all selected nodes within a dataset.
 * @param {VisDataSet} visGraphData The vis data set for the graph, containing its edges and nodes.
 * @param {jQuery} clearButtonEl The jquery element used to clear all selected nodes.
 */
export function clearSelectedNodes(visGraphData, clearEl) {
    let nodes = visGraphData.nodes.get().filter(n => n.color === Constants.VIS_NODE_COLOURS.SELECTED);
    let updates = [];
    nodes.forEach(n => {
        updates.push(updateNode(n));
    });
    $('#regulogGraph').removeClass('enabled');
    $('#regulogGraph').addClass('disabled');
    $('#pathway').removeClass('enabled');
    $('#pathway').addClass('disabled');
    $('#selected-label').text(selectedEmptyLabel);
    clearEl.hide();
    visGraphData.nodes.update(updates);
}

/**
 * Select the node of a regulon given its name.
 * @param {string} regulonName The name of the regulon that has been selected.
 * @param {RegulonNetwork} regulonNetwork The regulon network being searched.
 * @param {VisDataSet} visGraphData The vis graph data containing the nodes and edges of the graph.
 * @param {string} apiUrl The url of the api route module used for the graph.
 * @param {jQuery} clearButtonEl The jquery element used to clear all selected nodes.
 */
export function selectNode(regulonName, regulonNetwork, visGraphData, apiUrl, clearButtonEl) {
    let selectedNode = visGraphData.nodes.get().find(n => n.id === regulonName.toLowerCase());
    visGraphData.nodes.update(updateNode(selectedNode));
    let selectedRegulonNames = visGraphData.nodes.get().filter(n => n.color === Constants.VIS_NODE_COLOURS.SELECTED).map(n => n.label);
    let selectedRegulonIds = selectedRegulonNames.map(name => {
        return regulonNetwork.find(r => r.regulatorName.toLowerCase().replace(/[|&;$/%@"<>()+,]/g, '') === name.replace(/[|&;$%@"/<>()+,]/g, '').toLowerCase()).regulonId;
    });

    // let selectedRegulonIds = visGraphData.nodes.get().filter(n => n.color === selectedNodeColour).map(n => n.id);
    if (selectedRegulonIds.length > 0) {
        let pathwayHref = `${apiUrl}/pathway?regulons=${selectedRegulonIds.join('+')}`;
        $('#selected-label').text('Selected Regulons: ' + selectedRegulonNames.join(', '));
        $('#pathway').removeClass('disabled');
        $('#pathway').addClass('enabled');
        $('#pathway').attr('href', pathwayHref);
        clearButtonEl.show();
        if (selectedRegulonIds.length === 1) {
            let regulogHref = `${apiUrl}/reguloggraph?regulonId=${selectedRegulonIds[0]}`;
            $('#regulogGraph').removeClass('disabled');
            $('#regulogGraph').addClass('enabled');
            $('#regulogGraph').attr('href', regulogHref);
        } else {
            $('#regulogGraph').removeClass('enabled');
            $('#regulogGraph').addClass('disabled');
        }
    } else {
        $('#regulogGraph').removeClass('enabled');
        $('#regulogGraph').addClass('disabled');
        $('#pathway').removeClass('enabled');
        $('#pathway').addClass('disabled');
        $('#selected-label').text(selectedEmptyLabel);
        clearButtonEl.hide();
    }
}

/**
 * Update the graphs layout betwee heirachical and improved
 * @param {boolean} isEnabled Flag for whether to display hierarchical layout or not.
 * @param {VisNetwork} graph The vis graph to be updated.
 * @param {jQuery} dimmerEl The graph's dimmer element.
 */
export function toggleHierarchicalLayout(isEnabled, graph, dimmerEl) {
    // these are all options in full.
    var options = { layout: { hierarchical: { enabled: isEnabled } } };
    graph.setOptions(options);
    graph.fit();
    graph.stabilize(3000);
    if (!isEnabled) {
        dimmerEl.dimmer('show');
    }
}

/**
 * Clears the highlighted nodes for a graph.
 * @param {VisDataSet} visGraphData The vis data set containing the nodes and edges to be updated.
 */
export function clearHighLights(visGraphData) {
    let updates = [];
    visGraphData.nodes.get().forEach(node => {
        if (node.color && (node.color === Constants.VIS_NODE_COLOURS.HIGHLIGHTED)) {
            updates.push({
                id: node.id,
                color: Constants.VIS_NODE_COLOURS.DEFAULT
            });
        }
    });
    visGraphData.nodes.update(updates);
}

/**
 * Searches the names of target genes and regulators and highlights matched nodes.
 * @param {visJsNode} regulator The node being evaluated
 * @param {string} geneSearchTerm The name of the gene to be searched.
 * @param {string} tfSearchTerm The name of the transcription factor to be searched.
 */
export function completeSearch(regulator, geneSearchTerm, tfSearchTerm) {
    return !!((regulator && regulator.regulatorName) &&
        (
            (tfSearchTerm === '' || (tfSearchTerm && regulator.regulatorName.toLowerCase().includes(tfSearchTerm.toLowerCase().replace(/[|&;$%/@"<>()+,]/g, '')))) &&
            (geneSearchTerm === '' || (
                regulator.targetGenes.find(tg => tg.name && tg.name.toLowerCase().replace(/[|&;$%@"<>()+,\\\/]/g, '').includes(geneSearchTerm.toLowerCase())) ||
                regulator.targetRegulators.find(tr => tr.regulatorName && tr.regulatorName.toLowerCase().replace(/[|&;$%@"<>()+,\\\/]/g, '').includes(geneSearchTerm.toLowerCase()))
            ))
        )
    );
}

/**
 * Toggles the regulon graph to show the direction of edges.
 * @param {boolean} displayArrows Flag for whether or not to display directional arrows.
 * @param {VisDataSet} visGraphData The vis data set containing the required nodes and edges.
 */
export function toggleEdgeArrows(displayArrows, visGraphData) {
    let updates = [];
    let arrow = { to: { enabled: displayArrows } };

    visGraphData.edges.get().forEach(edge => {
        updates.push({ id: edge.id, from: edge.from, to: edge.to, arrows: arrow });
    });
    visGraphData.edges.update(updates);
}

/**
 * Find the nodes and edges for a regulon graph.
 * @param {RegulonNetwork} network The regulon network to be mapped.
 * @param {VisDataSet} visData The vis data set to be updated.
 */
function findGraph(network, visData) {
    network.forEach(regulator => {
        if (visData && visData.nodes && !visData.nodes.get().find(n => n.id === regulator.regulatorName.toLowerCase().replace(/[|&;$%@"<>()+,\\\/]/g, ''))) {
            visData.nodes.add({
                id: regulator.regulatorName.toLowerCase().replace(/[|/&;$%@"<>()+,]/g, ''),
                label: regulator.regulatorName,
                color: Constants.VIS_NODE_COLOURS.DEFAULT
            });
        }
        regulator.targetRegulators.forEach(tr => {
            if (tr.goTerm === undefined) {
                tr.goTerm = 'go term unknown';
            }
            let edgeColour = Constants.VIS_EDGE_COLOURS.GO_TERM_COLOURS[tr.goTerm] || Constants.VIS_EDGE_COLOURS.DEFAULT;
            const edge = {
                from: regulator.regulatorName.toLowerCase().replace(/[|&;$%@"<>()+,\\\/]/g, ''),
                to: tr.regulatorName.toLowerCase().replace(/[|&;$%@"<>()+,\\\/]/g, ''),
                color: { color: edgeColour, highlight: edgeColour, hover: edgeColour },
                effect: tr.effect,
                evidenceCodes: tr.evidence,
                strength: tr.strength,
                goTerm: tr.goTerm
            };
            if (!visData.edges.get().find(e => e.from === edge.from && e.to === edge.to)) {
                visData.edges.add(edge);
            }
        });
    });
}

/**
 * Updates a node that has been selected.
 * @param {VisNode} node The vis node to be updated.
 * @returns {NodeUpdate} The details of the node updates with the nodes id.
 */
function updateNode(node) {
    let update;
    if (!node) return;
    if (node.color && node.color === Constants.VIS_NODE_COLOURS.SELECTED) {
        update = { id: node.id, color: Constants.VIS_NODE_COLOURS.DEFAULT };
    } else {
        update = { id: node.id, color: Constants.VIS_NODE_COLOURS.SELECTED };
    }
    $(`#table #regulonTable tbody #${node.id}`).toggleClass('active');
    return update;
}

/**
 * Displays a tooltip with the regulons information.
 * @param {Regulon} hoveredRegulon The regulon being hovered.
 * @param {jQuery} tooltipEl The tooltip's jquery element to be updated.
 */
function createNewTooltip(hoveredRegulon, tooltipEl) {
    // Display a textbox containing the information above the node the users mouse is hovering over
    let table = $('<table>');
    if (hoveredRegulon.regulatorName) {
        table.append($('<tr>')
            .append($('<td>').text('Regulator Name:'))
            .append($('<td>').text(hoveredRegulon.regulatorName))
        );
    }

    if (hoveredRegulon.regulatorFamily) {
        table.append($('<tr>')
            .append($('<td>').text('Regulator Family:'))
            .append($('<td>').text(hoveredRegulon.regulatorFamily))
        );
    }

    if (hoveredRegulon.regulationType) {
        table.append($('<tr>')
            .append($('<td>').text('Regulation Type:'))
            .append($('<td>').text(hoveredRegulon.regulationType))
        );
    }
    if (hoveredRegulon.effector) {
        table.append($('<tr>')
            .append($('<td>').text('Pathway:'))
            .append($('<td>').text(hoveredRegulon.effector))
        );
    }
    if (hoveredRegulon.strengths) {
        table.append($('<tr>')
            .append($('<td>').text('Target Strengths:'))
            .append($('<td>').text(hoveredRegulon.strengths))
        );
    }
    if (hoveredRegulon.effects) {
        table.append($('<tr>')
            .append($('<td>').text('Target Effects:'))
            .append($('<td>').text(hoveredRegulon.effects))
        );
    }
    if (hoveredRegulon.evidenceCodes) {
        table.append($('<tr>')
            .append($('<td>').text('Target Evidence Codes:'))
            .append($('<td>').text(hoveredRegulon.evidenceCodes))
        );
    }

    tooltipEl.empty();
    tooltipEl.append(table);
}

/**
 *
 * Filters the displayed Regulons via all filter options
 * @param {string[]} selectedEvidenceCodes  The list of Evidence Codes being filtered.
 * @param {string[]} selectedRegulatoryEffects  The list of Regulatory Effects being filtered.
 * @param {boolean} selfRegulatingHidden The value to determine whether to filter self-regulating tfs
 * @param {boolean} unregulatedHidden The value to determine whether to filter unregulated tfs
 * @param {RegulonDbRegulonNetwork} network The regulon network to be mapped.
 * @param {VisDataSet} visGraphData The vis data set to be updated.
 * @param {boolean} onlySelfRegulating The value to determine whether to only filter by self-regulating tfs
 * @param {string} geneNameSearch The value to determine which gene Name to search by, if any
 * @param {string} tfNameSearch The value to determine which TF to search by, if any
 */
export function applyFilters(selectedEvidenceCodes, selectedRegulatoryEffects, network, visGraph, targetInput, regulatorInput, selfRegulatedState, unregulatedState, selectedGoTerm) {
    let edgeUpdates = [];
    let nodeUpdates = [];

    const nodes = visGraph.body.data.nodes.get();
    nodes.forEach(node => {
        const connectedEdgeIds = visGraph.getConnectedEdges(node.id);
        let connectedEdges = visGraph.body.data.edges.get(connectedEdgeIds);
        const regulator = network.find(r => r.regulatorName && r.regulatorName.replace(/[|&;$%@"<>()+,\\\/]/g, '').toLowerCase() === node.id);
        let displayNode = checkUnregulated(connectedEdges, unregulatedState) && checkSelfRegualted(connectedEdges, selfRegulatedState) && completeSearch(regulator, targetInput, regulatorInput);
        // Only check edges if the node is visible, otherwise theres no point
        if (displayNode) {
            connectedEdges = connectedEdges.map(edge => {
                let displayEdge = true;
                if ((!selectedEvidenceCodes || selectedEvidenceCodes.length === 0) && (!selectedRegulatoryEffects || selectedRegulatoryEffects.length === 0) && (!selectedGoTerm)) {
                    displayEdge = true;
                } else if (
                    (selectedEvidenceCodes && selectedEvidenceCodes.length > 0 && evidenceFilter(selectedEvidenceCodes, edge.evidenceCodes)) ||
                    (selectedRegulatoryEffects && selectedRegulatoryEffects.length > 0 && !(selectedRegulatoryEffects.includes(edge.effect))) ||
                    (selectedGoTerm && (!edge.goTerm || (edge.goTerm && selectedGoTerm !== edge.goTerm.replace(/\s/g, ''))))) {
                    displayEdge = false;
                }
                return {
                    id: edge.id,
                    hidden: !displayEdge
                };
            });
            edgeUpdates.push(...connectedEdges);
            displayNode = ((!selectedEvidenceCodes || selectedEvidenceCodes.length === 0) && (!selectedRegulatoryEffects || selectedRegulatoryEffects.length === 0) && (!selectedGoTerm)) ||
                (connectedEdges.find(e => !e.hidden) ||
                (selectedRegulatoryEffects && selectedRegulatoryEffects.length > 0 && !!regulator.effects.find(e => selectedRegulatoryEffects.includes(e))) ||
                (selectedEvidenceCodes && selectedEvidenceCodes.length > 0 && !!regulator.evidenceCodes.find(e => selectedEvidenceCodes.includes(e))));
        }

        if (displayNode) {
            nodeUpdates.push({ id: node.id, hidden: false, physics: true });
        } else { // If the node passed the filter but is hidden, unhide it
            nodeUpdates.push({ id: node.id, hidden: true, physics: false });
        }
    });

    // Update the graph
    visGraph.body.data.nodes.update(nodeUpdates);
    visGraph.body.data.edges.update(edgeUpdates);
}

/**
 * Check if a node is self regulated for resuming state
 * @param {visData} connectedEdges The connected edges of a node
 * @param {boolean} displayState The display state of the graph
 */
function checkSelfRegualted(connectedEdges, displayState) {
    let displayNode = true;
    switch (displayState) {
    case 'hide':
        displayNode = !(connectedEdges.find(e => e.to === e.from));
        break;
    case 'only':
        displayNode = !!(connectedEdges.find(e => e.to === e.from));
        break;
    }
    return displayNode;
}

/**
 * Check if a node is unregulated for resuming state
 * @param {visData} connectedEdges The connected edges of a node
 * @param {boolean} displayState The display state of the graph
 */
function checkUnregulated(connectedEdges, displayState) {
    let displayNode = true;
    switch (displayState) {
    case 'hide':
        displayNode = connectedEdges.length > 0;
        break;
    case 'only':
        displayNode = connectedEdges.length === 0;
        break;
    }
    return displayNode;
}

/**
 * Decide whether or not to filter evidence code
 * @param {string[]} selectedEvidenceCodes The selected evidence codes
 * @param {string[]} evidenceCodes The evidence codes
 */
function evidenceFilter(selectedEvidenceCodes, evidenceCodes) {
    let hide = true;
    for (let code of evidenceCodes) {
        if (selectedEvidenceCodes.find(c => c.toLowerCase() === code.toLowerCase())) {
            hide = false;
            break;
        }
    }
    return hide;
}
