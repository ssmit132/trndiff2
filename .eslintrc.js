module.exports = {
  'env': {
    'browser': true,
    'es6': true,
    'node': true,
    'jquery': true,
  },
  'extends': 'standard',
  'globals': {
    'Atomics': 'readonly',
    'SharedArrayBuffer': 'readonly'
  },
  'parserOptions': {
    'ecmaVersion': 2018,
    'sourceType': 'module'
  },
  'rules': {
    'indent': ['error', 4],
    'space-before-function-paren': ['error', 'never'],
    // 'no-unused-vars': ['error', { 'vars': 'local', 'args': 'all', 'ignoreRestSiblings': false }],
    'semi': [2, 'always'],
    'no-unused-vars': ["error", { "argsIgnorePattern":"^_", "varsIgnorePattern": "^_", 'vars': 'local', 'args': 'all', 'ignoreRestSiblings': false  }]
  }
};
